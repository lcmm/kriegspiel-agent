#ifndef CHESS_STATE_H
#define CHESS_STATE_H

#define ZOBRIST_SIZE 768
#define WHITE -1
#define BLACK 1

#include <string>
#include <vector>
using namespace std;

class ChessState;

struct Move
{
   pair<int,int> from;
   pair<int,int> to;
   bool castle;
   bool capture;
   int en_passant;
   bool passant;
   bool promotion;
};

struct moveResult{
   ChessState* state;
   bool capture;
   string to;
   Move move;
};

class ChessState{
public:
   static long int* seed;
   static long int* passant_seed;
   static long int* castling_seed;
   
   //Atributos
   bool toRemove = false;
   long int zobristKey;
   int illegal = 0;
   bool legal;
   bool capture = false;
   bool terminal = false;
   int winner = 0;

   //Destructor
   virtual ~ChessState() {};

   //Metodos
   //Seed initializer
   static void initSeed();
   //Marks the state for future removal. Used by containers
   void markForRemoval();
   //Function for the remove idiom
   static bool isReadyForRemoval(ChessState* s);
   //Generates the zobrist key with the given random seed
   virtual void generateZobristKey() = 0;
   //Applies the given SAN notation move to the ChessState
   virtual ChessState* apply(string move, int color, bool check = false) = 0;
   //Applies the given SAN notation move to the ChessState
   virtual ChessState* applyMove(Move move, int color, bool checkTerminal = false) = 0;
   //Generates a move from a string
   virtual Move genMove(string san, int color, string* p = NULL) = 0;
   //Determines if there are less than "illegal_moves" illegal moves in the
   //current state
   virtual bool lessIllegal(int illegal_moves) = 0;
   //Generates the legal states derived from this state
   virtual vector<moveResult> generateLegalStates(int color, bool checkTerminal = false) = 0;
   //Generates the posibles moves
   virtual vector<moveResult> generateMoves(int color) = 0;
   //Revisa si hay o no mate
   virtual bool checkMate(int color, string where = "") = 0;
   //Imprime el tablero
   virtual void print(int tab = 0) = 0;
   //Crea una copia del estado fisico
   virtual ChessState* copy() = 0;
   //Cuenta la cantidad de piezas de un color
   virtual int numPieces(int color) = 0;
   //Verifica si hay victoria material para un color
   virtual bool isMaterialWin(int color) = 0;
   
};

#endif
