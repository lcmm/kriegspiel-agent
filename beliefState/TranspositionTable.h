#ifndef TRANSPOSITION_TABLE_H
#define TRANSPOSITION_TABLE_H

#include "ChessState.h"

class TranspositionTable{
public:
   //Atributos
   ChessState **table;
   long int tableSize;

   //Destructor
   ~TranspositionTable();

   //Metodos
   void init(long int size);
   long int getHash(ChessState* s);
};

#endif
