#ifndef HELPER_STRUCTS_H
#define HELPER_STRUCTS_H

#include <string>
#include <vector>
using namespace std;

struct ResponseInfo{
   string percept;
   string checkList;
};

struct Play{
   string legalMove;
   ResponseInfo responseInfo;
   vector<string> illegalMoves;
   int illegalSize;
};

struct Turn{
   Play white;
   Play black;
};

#endif
