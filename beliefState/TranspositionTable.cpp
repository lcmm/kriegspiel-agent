#include "TranspositionTable.h"
#include <iostream>
using namespace std;

void TranspositionTable::init(long int size){
   table = new ChessState*[size];
   for(int i=0;i<size;++i){
      table[i] = nullptr;
   }
   tableSize = size;
}

TranspositionTable::~TranspositionTable(){
   delete[] table;
}

long int TranspositionTable::getHash(ChessState* s){
   return s->zobristKey % tableSize;
}
