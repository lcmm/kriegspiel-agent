#ifndef BELIEF_STATE_H
#define BELIEF_STATE_H

#include <string>
#include <vector>
#include <map>
#include "TranspositionTable.h"
using namespace std;

class BeliefState{
public:
   //Atributos
   vector<ChessState*> states;
   TranspositionTable transpositionTable;

   //Constructores
   BeliefState(long int size = 1000); 

   //Metodos
   bool isDuplicate(ChessState* s);
   void clean();
   void addState(ChessState* s);
   void addStates(vector<ChessState*>& v);
   void print(int tab = 0);
   ChessState* pop();
   bool empty();
};

#endif
