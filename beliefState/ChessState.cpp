#include "ChessState.h"
#include <random>
using namespace std;

void ChessState::markForRemoval(){
   toRemove = true;
}

bool ChessState::isReadyForRemoval(ChessState* s){
   return s->toRemove;
}

long int* ChessState::seed = new long int[ZOBRIST_SIZE];
long int* ChessState::passant_seed = new long int[8];
long int* ChessState::castling_seed = new long int[16];

void ChessState::initSeed(){
   random_device rd;
   default_random_engine rng(rd());
   uniform_int_distribution<int> uni(0,100000000);

   for(int i=0;i<ZOBRIST_SIZE;++i){
      seed[i] = uni(rng);
   }
   
   for(int i=0;i<8;i++){
      passant_seed[i] = uni(rng);
   }
   for(int i=0;i<16;i++){
      castling_seed[i] = uni(rng);
   }
      
}


