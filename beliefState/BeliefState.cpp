#include "BeliefState.h"
#include <algorithm>
#include <iostream>
using namespace std;

BeliefState::BeliefState(long int size){
   transpositionTable.init(size);
}

bool BeliefState::isDuplicate(ChessState* s){
   return (transpositionTable.table[transpositionTable.getHash(s)] != nullptr);
}

void BeliefState::clean(){
   for(ChessState* c : states){
      if(c->toRemove){
	 transpositionTable.table[transpositionTable.getHash(c)] = nullptr;
      }
   }
   states.erase(remove_if(states.begin(),states.end(),ChessState::isReadyForRemoval),states.end());
}

void BeliefState::addState(ChessState* s){
   if(!(isDuplicate(s))){
      states.push_back(s);
      transpositionTable.table[transpositionTable.getHash(s)] = s;
   }
}


void BeliefState::addStates(vector<ChessState*>& v){
   for(int i=0;i<v.size();++i){
      addState(v[i]);
   }
}

ChessState* BeliefState::pop(){
   ChessState* res = states[states.size()-1];
   states.pop_back();
   return res;
}

void BeliefState::print(int tab){
   for(int j=0;j<tab;j++){
      cout<<"\t";
   }
   cout << "PRINTING BELIEF STATE! SIZE: " << this->states.size() << endl;
   for(ChessState* c : this->states){
      c->print(tab);
   }
   for(int j=0;j<tab;j++){
      cout<<"\t";
   }
   cout << this->states.size() << endl;
}

bool BeliefState::empty(){
   return states.size() == 0;
}

//int main(){
  // return 0;
//}
