#! /bin/bash

#Memory Limit: 5500000

Verbose=false
caseFile=$1
resFile=res.txt
timeFile=time.txt
graphFile=$2
depth=$3
algorithm=$4
fail=0
success=0
timeSum=0
while getopts "vd:f:" opt
do
    case $opt in
	v ) Verbose=true ;;
	d ) Dir=$OPTARG ;;
	* ) echo '(╯°□°）╯︵ ┻━┻' 
	    exit
    esac
done

if $Verbose 
then
    make clean
    make
else
    printf 'compilando...'
    make clean > /dev/null
    make > /dev/null
    printf 'done!\n'
fi

totalFiles=$(wc -l $caseFile | awk '{print $1}')
fileCount=0
for file in $(cat $caseFile)
do
    fileCount=$(($fileCount + 1))
    perc=$(echo print $fileCount/$totalFiles | perl)
    output=$(./timeout -m 5500000 ./dfs.out $file $algorithm $depth > $resFile 2> $timeFile)
    solve=$(tail -n1 $resFile)
    stats=$(tail -n2 $timeFile | head -n1)
    time=$(tail -n1 $timeFile | head -n1)
    result="Solved!"

    printf $file    
    if [ "$solve" != $result ]
    then
	printf " : "
	printf "$result"
	printf " = "
	printf $solve
	printf " $stats"
	fail=$(($fail+1))
	
    else
	printf " : "
	printf "$result"
	printf " = "
	printf $solve
	success=$(($success+1))
    fi

    part1=$(echo $stats | awk '{ print $3}')
    timeSum=$(echo $timeSum + $part1 | bc)
    echo "$perc $timeSum" >> $graphFile
    
    if $Verbose 
    then
	read -p "Enter para continuar" reply
    fi
    printf "\n"

done
echo "done!" #1>&2
echo "fail = " $fail " success = " $success
rm $resFile
rm $timeFile
