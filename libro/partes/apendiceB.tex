\chapter{Notación PGN}
\label{apendiceB}
\lhead{Apéndice B. \emph{Notación PGN}}

PGN, Portable Game Notation por sus siglas en inglés, es el formato de escritura
de juegos de ajedrez más usado. A continuación se describe la manera en que está
estructurada esta notación, concluyendo con las modificaciones necesarias para
poder expresar una partida de Kriegspiel.

\section{Cabecera}

Antes de especificar cualquier movimiento, un archivo en notación PGN comienza
con una serie de lineas de pares nombre-valor que describen detalles generales
de la partida jugada. Puede haber cualquier cantidad de líneas de cabecera, pero
en general se requieren que estén presentes las siguientes 7:

\begin{itemize}

\item \emph{Evento}: El nombre del evento o torneo en el que estuvo involucrada
la partida
\item \emph{Lugar}: Lugar donde la partida fue jugada
\item \emph{Fecha}: Fecha de inicio de la partida
\item \emph{Ronda}: El numero de ronda de la partida dentro del evento
\item \emph{Blancas}: Nombre del jugador de las blancas en formato
''nombre,apellido''
\item \emph{Negras}: Nombre del jugador de las negras
\item \emph{Resultado}: Resultado del juego. 1-0 significa victoria para las
blancas, 0-1 victoria para las negras, 1/2-1/2 tablas y * algún otro estado.

\end{itemize}

\section{Movimientos}

Luego de las cabeceras se pasa a los movimientos de la partida. Cada línea
consiste en el número de turno y dos movimientos: el de las blancas y el de las
negras.

Cada movimiento consiste en lo siguiente, en orden:

\begin{itemize}
\item Abreviación de una letra del nombre de la pieza. Vacío en el caso de los
peones
\item Número de fila o columna de la pieza en caso de que sea necesario
desambiguar la pieza a la que se hace referencia
\item 'x' si hubo una captura.
\item Abreviación de dos letras de la casilla a la que fue movida la pieza
\item '=' seguido de la abreviación de una pieza. Esto es sólo para cuando
ocurre una promoción de peón
\item '+' si hubo jaque. '\#' en caso de que sea mate
\end{itemize}

\section{Kriegspiel}

Hay dos tipos de archivo Kriegspiel PGN: filtrado y no filtrado. El no filtrado
no es más que lo que el juez ve, lo cual equivale a una partida de ajedrez
común, por lo que no se necesita agregar nada a la notación.

Las partidas filtradas representan la partida desde el punto de vista de uno de
los dos jugadores. Los movimientos del jugador desde el cual se está viendo la
partida se denotan con una línea de la forma:

\textless legal\textgreater\{(\textless respuesta\textgreater:\textless
ilegales\textgreater)\textless comentarios\textgreater\}

\textless legal\textgreater\ es el movimiento final ejecutado por el jugador, 
\textless respuesta\textgreater\ es el percepto del juez, \textless
ilegales\textgreater\ es una lista separada por comas de los movimientos fallidos y \textless 
comments\textgreater\ son comentarios.

La respuesta del juez consiste en lo siguiente, en orden:

\begin{itemize}
\item 'X' seguido de una casilla si hubo captura
\item 'C' seguido de una o dos letras si hubo jaque. Los valores de dichas
letras pueden ser 'F' para columna, 'L' para diagonal larga, 'N' para caballo,
'R' para fila y 'S' para diagonal corta. El largo de las diagonales se determina
desde el punto de vista del rey en jaque
\end{itemize}

Los movimientos del jugador invisible se representan de manera similar, excepto
que el movimiento legal es reemplazado por ''??'' y de los movimientos ilegales
sólo se conoce su cantidad.

Adicionalmente, se agregan 11 nuevas líneas de cabecera especialmente creadas
para instancias de Kriegspiel a ser resueltas por computadora. Algunas de estas
líneas tienen valores booleanos, determinados por ''T'' o ''F'':

\begin{itemize}
\item \emph{try-until-feasible}: Valor booleano que determina si la partida en 
cuestión obedece la regla ''try-until-feasible'', la cual permite que un jugador 
intente movimientos posiblemente ilegales hasta conseguir uno que sea legal.
Esto es siempre verdad para Kriegspiel.
\item \emph{filtered}: Determina el jugador hacia el cual el historial está
filtrado. Puede ser ''white'' o ''black''.
\item \emph{depth}: El número de movimientos necesarios para que el jugador
filtrado logre mate. Para instancias de profundidad 3, el valor es 2. Para las
de profundidad 5, es 3. Esto quiere decir que no se cuenta la profundidad del
árbol.
\item \emph{forced}: Valor booleano que determina si se consideran los mates
materiales como mates inmediatos. Siempre es verdad para las instancias
manejadas.
\item \emph{epsilon}: Valor booleano que determina si los mates materiales con
probabilidad 1-epsilon de llegar a mate se consideran mates inmediatos. Siempre
es verdad para Kriegspiel. Implica que \emph{forced} también es verdad.
\item \emph{computed-states}: La cantidad de estados físicos generados en el
proceso de estimación a lo largo del historial.
\item \emph{final-states}: La cantidad de estados físicos que el jugador
filtrado posee en su estado de creencia al final del historial.
\item \emph{mate-states}: La cantidad de estados físicos finales que admiten un
mate en Kriegspiel. Para las instancias mate, \emph{final-states} es igual a
este valor.
\item \emph{largest-mate-subset}: Una aproximación a la cantidad de estados en
el estado de creencia que admiten mate. No se obtiene la cantidad exacta por ser
un problema computacionalmente difícil. Para las instancias mate,
\emph{final-states} es igual a este valor.
\item \emph{white-pcs}: Cantidad de piezas blancas al momento de finalizar el
historial.
\item \emph{black-pcs}: Cantidad de piezas negras al momento de finalizar el
historial.
\end{itemize}

\begin{figure}
\centering
\label{imagen:sample}
\begin{BVerbatim}
[event "Checkmate Problem"]
[site "UC Berkeley"]
[date "2004.12.19"]
[round "1"]
[white "Computer Player"]
[black "Computer Player"]
[result "*"]
[variant "Kriegspiel (Berkeley)"]
[try-until-feasible "T"]
[filtered "white"]
[depth "2"]
[forced "T"]
[epsilon "T"]
[computed-states "475"]
[final-states "2"]
[mate-states "2"]
[largest-mate-subset "2"]
[white-pcs "9"]
[black-pcs "4"]

1.  d4 {(:)}
    ?? {(:0)}
2.  dxe5 {(Xe5:dxc5)}
    ?? {(CL:0)}
3.  c3 {(:)}
    ?? {(Xc3,CL:1)}
4.  bxc3 {(Xc3:)}
    ?? {(:0)}
5.  Nf3 {(:f4)}
    ?? {(Xf2,CS:1)}
6.  Kxf2 {(Xf2:)}
    ?? {(:2)}
7.  Qxd5 {(Xd5:Qd7)}
    ?? {(:3)}
8.  Qxb7 {(Xb7:h4)}
    ?? {(Xg2:0)}
9.  Qxa8 {(Xa8:)}
    ?? {(Xf3:2)}
10.  exf3 {(Xf3:)}
     ?? {(:1)}
\end{BVerbatim}
\caption{Ejemplo de una instancia de Kriegspiel.}
\end{figure}
