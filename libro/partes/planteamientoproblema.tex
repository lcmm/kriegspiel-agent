\chapter{Planteamiento del Problema}
\label{capitulo1}
\lhead{Capítulo 3. \emph{Planteamiento del Problema}}

El problema central a resolver con este trabajo radica en la replicación de
parte del trabajo hecho por Russell y Wolfe. Antes de comenzar a pensar en cómo
lograr esto, hay que ver primero en que consistió la investigación mencionada y
cuáles fueron sus resultados.

El trabajo de Russell y Wolfe (de ahora en adelante referido como \emph{trabajo
de Berkeley}) consistió en el desarrollo y análisis de nuevos algoritmos para la
resolución de juegos de información incompleta. La idea central de estos nuevos
algoritmos es el uso de una dimensión de búsqueda más alla de la profundidad o
anchura: la incertidumbre, lo cual no es más que la construcción de sucesores de
estados físicos dentro de los estados de creencia de manera incremental. Al
terminar con su trabajo, los investigadores de Berkeley consiguen mejorar el
desempeño de la búsqueda por dos o más ordenes de magnitud en instancias
difíciles de Kriegspiel. Esto por supuesto en comparación con algoritmos de
búsqueda AND-OR clásicos.

\section{Estructuras}

Antes de describir los algoritmos implementados en el trabajo de Berkeley, hay
que especificar la estructura sobre la cual la búsqueda fue hecha. Primero se
tiene el estado de creencia, el cual viene a ser un nodo del árbol, ya sea
AND, OR o EXPAND (La diferencia entre estos tres tipos de nodos será explicada
más adelante). Este nodo contiene los estados físicos que se cree son los posibles
para ese momento en el árbol de jugadas. Los estados físicos como tales, su
estructura y cómo están ordenados dentro del estado de creencia pueden ser
implementados de varias formas. Una descripción detallada de nuestra
representación para nuestros estados físicos y de creencia está disponible en el
Capítulo 4.

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{estadodecreencia.png}
\caption{Ejemplo de estado de creencia con 3 estados físicos
\cite{wolfeRussell}}.
\label{imagen:estadodecreencia}
\end{figure}

Luego se tiene el árbol de búsqueda AND-OR-EXPAND. Este árbol tiene tres tipos
de nodo: AND, OR y EXPAND. El objetivo final de todos los algoritmos de búsqueda
de Berkeley es probar el árbol como verdadero (Hay un plan de jugadas que lleva
a victoria para las blancas) o falso (No hay dicho plan). Para esto se debe
probar o refutar la raíz del árbol. Cada tipo de nodo tiene diferentes
condiciones para determinar sus sucesores y valor final.

Los nodos OR representan el turno de las blancas, y contienen estados de creencia 
cuyos estados físicos están esperando por un movimiento de las blancas. Cada estado 
físico en un Nodo OR posee las mismas posiciones para las piezas blancas, 
diferenciandose solamente en el estado de las negras. En otras palabras, los
nodos OR son una representación de una decisión de las blancas. Los nodos OR son
probados si y sólo si al menos uno de sus hijos es probado. Los hijos de un nodo
OR son nodos AND, que se corresponden con los resultados de las posibles jugadas
de las blancas.

Los nodos EXPAND representan el turno de las negras, con contenido similar en
los estados físicos de los nodos OR. Sin embargo, como los movimientos de las
negras son invisibles para las blancas, los nodos EXPAND poseen tan sólo un hijo
AND, que contiene todos los estados físicos resultantes de aplicar todos los
movimientos posibles de las negras en todos los estados físicos del nodo EXPAND.
Un nodo EXPAND es probado si y sólo si su hijo es probado.

Los nodos AND representan la llegada de un percepto del árbitro, y puede ser
terminal o no terminal, dependiendo del estado del movimiento del cual fue
originado. Si todos los estados físicos del nodo AND representan una victoria
para las blancas, el nodo es terminal con valor verdadero. Si algún estado
físico es tablas o derrota para las blancas, el nodo es terminal con valor falso.
De otro modo, el nodo AND es no terminal, y tiene hijos que forman una partición
de sus estados físicos no terminales. Esto quiere decir que un nodo AND tiene
un hijo para cada tipo de percepto. Hay un caso especial para los hijos AND de
nodos OR. El percepto de ilegal es llevado a un hijo OR con la misma
profundidad. El resto de los hijos son nodos EXPAND. Un nodo AND es probado si y
sólo si todos sus hijos son probados y además todos sus estados son una victoria
para las blancas.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth]{andorexpand.png}
\caption{Ejemplo de un árbol AND-OR-EXPAND \cite{wolfeRussell}}.
\label{imagen:andorexpand}
\end{figure}

La estructura final de un árbol AND-OR-EXPAND queda entonces de la siguiente
manera: la raíz es un nodo OR, seguido de nodos AND y luego nodos EXPAND, para
repetir así el ciclo. Potencialmente existen ramas largas de nodos OR luego de nodos
AND, correspondientes a los perceptos ilegales. Cabe destacar que este es un
árbol implícito. Esto quiere decir que cada algoritmo lo irá generando a medida
que se vaya explorando.

En la figura 3.2 se tiene un ejemplo de dicha estructura. La raíz del árbol es
un nodo OR con tres estados físicos. Sus ramas son por movimiento y llevan a los
nodos AND, los cuales son los nodos delgados que aparecen cada otro nivel.
Luego, los nodos AND se ramifican por percepto, como se puede ver en la
ramificación del nodo 2 en ilegal y jaque mate. El nodo 5 es un nodo EXPAND
generado por el nodo AND al tener el percepto legal de captura en a3. Cabe
destacar cómo los nodos OR corresponden a las blancas, los EXPAND a las negras y
el AND al juez.

\section{Algoritmos Clásicos}

Ahora, con la estructura definida, se puede pasar a describir los algoritmos
propuestos en el trabajo de Berkeley. Primero se tiene el clásico algoritmo DFS,
el cual básicamente consiste en expandir el nodo actual y determinar si con la
información del nodo y sus hijos se puede probar el árbol. En caso contrario, se
hace una llamada recursiva por cada hijo para intentar probarlos y asi usar ese
resultado para probar o refutar el nodo actual. Este algoritmo resulta ser la
base principal de los algoritmos incrementales que se describirán más tarde, así
que una especificación más detallada es necesaria.

El procedimiento principal en DFS es SOLVE-TOP, corrido inicialmente en la raíz
del árbol. SOLVE-TOP simplemente expande el nodo actual para generar sus hijos y
luego determina su valor final con el procedimiento SOLVE. SOLVE posee una
instancia separada para cada tipo de nodo. Para los nodos OR, SOLVE corre
SOLVE-TOP en cada uno de los hijos del nodo OR hasta conseguir uno que sea
probado. En caso de conseguir uno, el nodo OR es considerado como probado. En
caso contrario retorna false. Para los nodos EXPAND, SOLVE simplemente consiste
en llamar SOLVE-TOP en el único hijo del nodo. Para los nodos AND, primero se
ve si el nodo es terminal. En ese caso simplemente se retorna su valor actual.
En caso contrario se corre SOLVE-TOP en los hijos hasta conseguir uno que no sea
probado. En ese caso el nodo AND se considera como no probado. Si no pasa esto,
se considera el nodo AND como probado.

\begin{figure}
\centering
\label{imagen:solvetop}
\begin{BVerbatim}
boolean SOLVE-TOP(Node root)
   EXPAND(root)
   return SOLVE(root)

boolean SOLVE(OrNode root)
   for(Node child in CHILDREN(root))
      if(SOLVE-TOP(child)) return true
   return false

boolean SOLVE(AndNode root)
   if(root.terminal) return root.value
   for(Node child in CHILDREN(root))
      if(not SOLVE-TOP(child)) return false
   return true

boolean SOLVE(ExpandNode root)
   return SOLVE-TOP(FIRST(root.children))
\end{BVerbatim}
\caption{Pseudocódigo de las instancias de SOLVE y SOLVE-TOP.}
\end{figure}


El procedimiento para generar los hijos de cada nodo es llamado EXPAND, y
también tiene una instancia para cada tipo de nodo. Para los nodos OR, EXPAND
consiste en aplicar todos los movimientos posibles para uno de los estados
físicos (que a la vez vendrían siendo también posibles en los demás estados, por
lo menos en lo que respecta a la posición de las blancas) a todos los estados
físicos del nodo. Se crea entonces un nodo AND por cada movimiento, con aquellos
estados (para ese movimiento) que no representen una victoria para las blancas.
Sin embargo, si hay un solo estado tal que es terminal (o la profundidad actual
es 1) y no hay victoria para las blancas, se interrumpe la creación del nodo y
no se considera este movimiento. Un nodo hijo AND es marcado como terminal si
termina por no poseer estados físicos (Lo cual en este caso sólo llega a pasar
si todos los estados representan una victoria para las blancas).

\begin{figure}
\centering
\label{imagen:solvetop}
\begin{BVerbatim}
void EXPAND(OrNode node)
   for(MOVE move in FIRST(node.states))
      b = new AndNode
      b.terminal = false
      b.value = true
      b.depth = node.depth
      b.children = empty list
      b.states = MAP(APPLYMOVE(m),node.states)
      for(State state in b.states)
	 if(WINNER(s) == WHITE) b.states.remove(s)
	 else if(s.terminal or b.depth == 1)
	    b = null
	    break
      if(b)
	 node.states.push(b)
	 if(b.states.empty)
	    b.terminal = true
	    break
\end{BVerbatim}
\caption{Pseudocódigo de la instancia OR de EXPAND.}
\end{figure}


En el caso de los nodos EXPAND, el procedimiento EXPAND genera un único nodo
AND, el cual contiene todos los estados físicos resultados de todos los
movimientos posibles de las negras. El nodo se marca como terminal en el momento
en que se consiga un estado físico terminal, y se coloca su valor como falso.

Para los nodos AND, los hijos son EXPAND o OR dependiendo de a quien le toca
jugar en el momento. Los hijos se dividen por los distintos perceptos dados por
el juez. En el caso de las jugada de las blancas, los hijos con perceptos
legales son nodos EXPAND, mientras que se mantiene un hijo OR (de la misma
profundidad) para el percepto ilegal.

A pesar de que DFS es la base de la investigación de Berkeley, también
implementaron y analizaron el algoritmo PNS, el cual expande el nodo 
\emph{más probante (most proving)}, es decir, aquél que provee la mayor contribución a la hora
de probar o refutar el árbol. Sin embargo, el trabajo no ahonda mucho en los
resultados o posibles expansiones de este algoritmo, a pesar de que termina
teniendo un mejor desempeño que DFS.

Antes de pasar a los algoritmos incrementales, Russell y Wolfe notan que se
puede conseguir una mejora de velocidad al elegir inteligentemente qué nodo
expandir primero entre los posibles en cada iteración. En particular, expandir
los nodos legales antes del ilegal es mucho más barato en tiempo, pues la
profundidad y ramificación potencial del subárbol ilegal es mucho mayor que la
de sus homólogos legales. Otra técnica para aumentar la velocidad del algoritmo
está basada en un teorema desarrollado en la investigación: \emph{Si un estado
de creencia no admite jaque mate asegurado, no hay super set de dicho estado que
lo admita}. En este teorema es que se basa el método EXPAND para eliminar nodos
hijos si se consigue un estado físico que represente una derrota para las
blancas o un estado no terminal al límite de profundidad, tal como es hecho en
la línea 11 del pseudocódigo.

\section{Algoritmos Incrementales}

El teorema previamente descrito es la idea desde la cual Russell y Wolfe
desarrollan los algoritmos incrementales: algoritmos basados en expandir los
estados físicos de un estado de creencia uno por uno en lugar de todos en
conjunto. Al tomar en cuenta el posible árbol de expansiones que cada estado
físico tiene por su cuenta, se está creando una dimensión de búsqueda más allá
de la profundidad o la anchura. Esta dimensión es llamada \emph{incertidumbre}
por los investigadores de Berkeley.

En el trabajo de Berkeley se consideraron tres algoritmos que usan la
incertidumbre al probar o refutar árboles de búsqueda: UDB, DBU y DUB. La
diferencia entre los tres radica en qué dimensión considerar primero, ya sea U
de \emph{Uncertainty}, D de \emph{Depth} o B de \emph{Breadth}.

El primer algoritmo considerado fue UDB, pero al final se concluye que no es más
que E-DFS, es decir DFS con la técnica de mejora de desempeño de eliminar a un
hijo si uno de sus estados físicos es un empate o derrota para las blancas. Es
por esto que no se ahonda mucho en el análisis de este algoritmo.

En cambio se tiene DBU y DUB, que al considerar la profundidad primero, son
capaces de expandir los árboles de estado de un estado físico a la vez, al
expandir el estado físico que esté a mayor profundidad en el árbol de prueba de
estados físicos actual. La diferencia entre DBU y DUB es que DUB se limita a
expandir sólo aquellos estados físicos en la rama de estados de creencia actual,
mientras que DBU sólo se preocupa por el estado físico a mayor profundidad,
independientemente del estado de creencia al que pertenezca.

\begin{figure}
\centering
\label{imagen:solvetop}
\begin{BVerbatim}
void INCREMENTAL-EXPAND(OrNode node, State s)
   if(node.children.empty)
      for(Move m in MOVES(s))
	 b = new AndNode
	 b.terminal = true
	 b.value = true
	 b.depth = node.depth
	 b.children = empty list
	 b.move = m
	 b.states = empty luist
      node.states.push(b)
   for(b in node.children)
      st = APPLYMOVE(b.move,s)
      if(st.terminal or b.depth == 1)
	 if(not st.terminal or WINNER(st) != WHITE)
	    node.states.remove(b)
      else
	 b.states.push(st)
	 b.terminal = false

boolean SOLVE-TOP(Node root)
   while(not root.empty)
      INCREMENTAL-EXPAND(root.states.pop())
      if(not SOLVE(root)) return false
   return true
		  
\end{BVerbatim}
\caption{Pseudocódigo de la instancia OR de INCREMENTAL-EXPAND y SOLVE-TOP.}
\end{figure}


DBU se basa inicialmente en DFS, usando las mismas instancias del procedimiento
SOLVE, pero con un SOLVE-TOP y un procedimiento de expansión de nodos distinto,
llamado INCREMENTAL-EXPAND. INCREMENTAL-EXPAND también posee instancias para
cada tipo de nodo, y su diferencia con el EXPAND anterior es que sólo expande un
estado físico por llamada, usando los mismos criterios para generar hijos y
marcarlos como terminales (excepto que la marca de terminal sólo aplica cuando
ya todos los estados físicos han sido vistos). Es por esto que el nuevo SOLVE-TOP 
se encarga de llamar a INCREMENTAL-EXPAND y luego a SOLVE una vez por cada estado 
físico en el estado de creencia.

Por otro lado se tiene DUB, que se basa en DBU inicialmente. Se trata de dos
recursiones: SOLVE-TOP (idéntico al de DBU) y OUTER. Estas dos recursiones se
llaman en el nuevo procedimiento raíz: OUTER-TOP. En este caso la instancia para
nodos AND de SOLVE tan solo llama a SOLVE-TOP para el primer hijo del nodo, para
mantenerse en la misma rama de estados de creencia. OUTER se encarga de
verificar la prueba en la rama actual y de completarla con el resto de las
ramas.

\begin{figure}
\centering
\label{imagen:solvetop}
\begin{BVerbatim}
boolean OUTER-TOP(Node root)
   return SOLVE-TOP(b) and OUTER(b)

boolean OUTER(OrNode root)
   while(not root.children.empty)
      if(OUTER(FIRST(root.children))) return true
      root.children.pop()
      if(not SOLVE(root)) return false
   return false

boolean OUTER(ExpandNode root)
   return OUTER(FIRST(root.children))

boolean OUTER(AndNode root)
   if(root.terminal) return root.value
   while(not root.children.empty)
      if(not OUTER(FIRST(root.children))) return false
      root.children.pop()
      if(root.children.empty) return true
      if(not SOLVE(root)) return false
   return true

boolean SOLVE(AndNode root)
   if(root.terminal) return root.value
   return SOLVE-TOP(FIRST(root.children))
\end{BVerbatim}
\caption{Pseudocódigo de las recursiones de DUB.}
\end{figure}

Los resultados obtenidos por los algoritmos incrementales reportan una mejora de
desempeño significativa en comparación con los algoritmos clásicos, resolviendo
instancias difíciles de Kriegspiel en menos de la mitad del tiempo usado por
algoritmos como DFS y PNS. Hay que notar que para estas instancias hubo un
algoritmo incremental adicional que tuvo aún mejor desempeño en las instancias:
IPNS, o PNS incremental. En este trabajo no consideramos este algoritmo, pues el
enfoque es en la teoría de la dimensión de búsqueda de la incertidumbre y las
ganancias de IPNS son marginales (e incluso peores en otros tipos de instancia).

\section{Requerimientos Previos}

Ya con el trabajo de Berkeley explicado, se puede pasar a replicarlo y analizar
sus resultados. Para hacer esto se necesita una cantidad considerable de trabajo
previo, el cual será explicado a continuación.

Lo primero que se necesita es una base de datos de instancias de problemas de
Kriegspiel. Afortunadamente, los problemas usados en el trabajo de Berkeley
estan disponibles libremente, y están completamente documentados. La notación de
dichas instancias se encuentra específicada en el apéndice A.

En rasgos generales, una instancia de Kriegspiel consiste en un historial de
movimientos de una partida de Kriegspiel en notación PGN (con los movimientos de 
las negras sin especificar). La notación PGN es la utilizada comúnmente en los
historiales de partidas de ajedrez para especificar movimientos. Una descripción
más detallada de esta notación, además de los cambios hechos para Kriegspiel se
puede encontrar en el apéndice B.

Todas las instancias se enfocan en las blancas y se dividen en dos
tipos: mate y nomate. la diferencia es que en las instancias mate siempre hay
un plan condicional que lleva a la victoria, independientemente del estado del
oponente. En las instancias nomate hay estados desde los cuales no existe dicho
plan, por lo que en términos generales una instancia nomate tomará más tiempo en
ser resuelta, pues existen más caminos a seguir en el árbol que no llevan a un
plan satisfactorio.

Ya con las instancias en mano, es imperativo proceder al diseño e implementación
de una representación explícita de los estados físicos de Kriegspiel que sea lo
suficientemente flexible para que, al cambiar su funcionamiento interno, no haya
necesidad de cambiar los algoritmos que la usen.

Luego se necesita implementar los estados de creencia. Una característica
fundamental de dichos estados es que sean capaces de eliminar estados duplicados
en caso de que estos aparezcan y de que se pueda determinar el tipo de nodo de
árbol AND-OR-EXPAND que se está tratando sin perder el funcionamiento general
del mismo.

Finalmente hace falta implementar un estimador que sea capaz de leer los movimientos
del historial de una instancia de Kriegspiel y determinar el estado de creencia
inicial de la misma. Para este trabajo sólo se han tomado en cuenta aquellas
instancias que no posean movimientos de enroque o promoción de parte de las
blancas, pues no se considera estrictamente necesario para el estudio de los
algoritmos de búsqueda propuestos.

Ya después de tener estos requerimientos, se puede finalmente pasar a
implementar los algoritmos DFS, DBU y DUB. Hay una buena cantidad de detalles de
implementación que se han pasado por alto en la descripción teórica de estos
algoritmos. En el capítulo que viene a continuación estos detalles serán
revelados, luego de explicar la implementación de los requerimientos previos.
