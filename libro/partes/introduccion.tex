\chapter{Introducción}
\label{intro}
\lhead{\emph{Introducción}}
\addcontentsline{toc}{chapter}{Introducción}

%% Descripcion de Kriegspiel

Desde el comienzo de nuestros tiempos, los juegos han sido una reflexión de
nuestra forma de ver la vida. Su significado y utilidad para resolver los
problemas de nuestro entorno ha sido invaluable para la raza humana a través de
los años.

Es por esto que muchos investigadores se han dedicado a estudiar los juegos, su
significado y cómo resolverlos, especialmente aquellos que simulan problemas de
alta importancia para nosotros.

No es de sorprender entonces que gran parte del trabajo en inteligencia
artificial de hoy en día se base en resolver una amplia variedad de juegos y
sacar conclusiones y recomendaciones sobre los resultados de dichas
investigaciones. En particular se ha tenido mucho éxito resolviendo juegos con
información completa: aquellos en los cuales toda la información necesaria
para resolver el juego está presente desde el inicio y todo cambio es visible
para el jugador.

Sin embargo, esta clase de juegos tiene limitantes a la hora de conseguir
aplicaciones para la vida real, pues raramente se tiene toda la información
necesaria en todo momento. A los juegos que cumplen con esta condición se les
llama juegos con información incompleta, y son notoriamente más difíciles de 
resolver computacionalmente, ya que requieren de algo bastante complicado de
simular artificialmente: el sentido común.

En este trabajo se estudia un juego en particular de información incompleta
llamado Kriegspiel. Kriegspiel es una variante del ajedrez inventada por Michael 
Temple en 1899, cuya principal diferencia con el ajedrez convencional es que el 
jugador solo conoce la posicion de sus piezas mientras las piezas del oponente 
estan a oscuras. Esta variante cuenta con un árbitro, encargado de validar los
movimientos de ambos jugadores, los comentarios del mismo son escuchadas por 
ambos jugadores. Un jugador de Kriegspiel tiene que ser capaz de advinar el estado 
completo del tablero asi como el camino hasta un jaquemate solo con sus movimientos 
y las respuestas del arbitro.

Kriegspiel es un caso ideal de un juego de información incompleta, pues es lo
suficientemente complejo como para requerir un razonamiento considerable antes
de cada jugada: Se debe razonar acerca de los movimientos hechos con
anterioridad, el posible estado actual del tablero, y los posibles resultados de
una jugada. Más aún, no hay garantía de la existencia de un plan perfecto para
conseguir la victoria en una partida, dando más importancia al verdadero
razonamiento en lugar de simplemente ver los resultados de experimentar con
todas las posibilidades y escoger la mejor jugada. En este trabajo se analizarán
técnicas para lograr simular dicho razonamiento.

Existen muchas variantes regionales de Kriegspiel, que aunque en esencia son iguales, pueden
tener ligeras diferencias a las definidas en este trabajo. La variante usada
es conocida como la variante "Berkeley", usada por Stuart Russell y Jason Wolfe 
en su tesis sobre este tema \cite{wolfeRussell}.

Este trabajo está estructurado como se describe a continuación. En este capítulo
se describen los antecedentes de este proyecto, además de sus objetivos
generales y específicos.

En el capítulo 2 se habla del marco teórico de este proyecto, describiendo a 
profundidad el estado del arte en investigación de juegos con información completa e
incompleta, además de todos los conceptos y algoritmos que formarán la base de
conocimiento necesaria para el completo entendimiento de nuestros enfoques y
resultados.

En el capítulo 3 se plantea el problema que pretendemos resolver. En este
proyecto se quiere replicar el trabajo hecho por Russell y Wolfe
\cite{wolfeRussell} para probar el desempeño y utilidad futura de sus algoritmos 
incrementales. Para lograr esto, una serie de requerimientos previos deben ser logrados, 
incluyendo la necesidad de crear un estimador que tome un historial de una partida de 
Kriegspiel incompleta y determine el estado de creencia del agente al final de la misma,
para  luego pasar a implementar y probar el enfoque incremental tomando como estado
inicial de creencia el obtenido por el estimador.

En el capítulo 4 se describe nuestra implementación de algunos de los algoritmos
descritos en el trabajo de Russell y Wolfe. Dicha descripción incluye un breve
repaso de las reglas de Kriegspiel importantes que se deben tomar en cuenta en
la implementación, además de una descripción de la representación usada tanto
para un estado físico como un estado de creencia, y la descripción del tipo de
árbol AND-OR que será recorrido por los algoritmos. Finalmente se describen tres
algoritmos: DFS (para su comparación con los algoritmos incrementales), DBU y
DUB. Para estos algoritmos se describe cómo funcionan, la idea detrás del
algoritmo, por qué funcionan y se su implementación.

En el capítulo 5 se presentan los resultados de correr los algoritmos
implementados en una base de datos de problemas considerados como difíciles,
requiriendo al menos 5 jugadas para ser resueltos. Se compara la correctitud y
desempeño de dichos algoritmos.

En el capítulo 6 se dan las conclusiones obtenidas del desempeño relativo de los
algoritmos y los posibles enfoques futuros que se pueden perseguir basándose en
los resultados. En particular se habla de algunos avances basados en Monte Carlo
descritos por Favini y su comparación con el trabajo de Russell y Wolfe, además
de la posibilidad de usar una representación factorizada de los estados de
creencia que sea más eficiente a la representación explícita utilizada en este
trabajo.

%en donde cada estado físico representa sólo una fracción de un estado
%real. Esta representación podría aumentar la eficiencia de los algoritmos dados
%a cambio de un uso mayor de espacio.

%% Antecedentes
\section{Antecedentes}

En los ultimos 10 años ha existido una amplia variedad de trabajos relacionados
con Kriegspiel y el desarrollo de algoritmos para su resolución. En esta seccion nos 
limitaremos a hablar de aquellos trabajos que representan el estado del arte
en el área y que tienen relevancia con los objetivos principales de este
trabajo.

Primero, se hablará de \emph{Efficient belief-state AND–OR search, with
application to Kriegspiel} [Russell y Wolfe, 2009]. Este trabajo
está intimamente relacionado al actual ya que expone parte del estado del arte de
algoritmos de resolucion para juegos con informacion incompleta, en particular
la técnica que se pretende replicar en este proyecto.

Russell y Wolfe \cite{wolfeRussell} exponen una forma eficiente de representación de los estados de
creencia de una partida de Kriegspiel, y a partir de allí prueban y desarrollan 
algoritmos de búsqueda sobre árboles AND-OR especiales. Luego de determinar que la
eficiencia de algoritmos clásicos como DFS y PNS no es la mejor, Russell y Wolfe
desarrollan versiones incrementales de estos algoritmos, los cuales usan la
incertidumbre como una nueva dimensión de búsqueda. Esto termina resultando en
una mejora de hasta dos órdenes de magnitud en comparación al desempeño de los
algoritmos clásicos en instancias de partidas consideradas como difíciles.

Luego se tiene a Favini \cite{favini}, con un trabajo que da una visión global del estado del arte en la
investigación de resolución de juegos tanto de información completa como
incompleta. Favini entonces se encarga de describir Kriegspiel y sus iteraciones
en la creación de un solver para el juego. Primero intenta un algoritmo basado
en minimax con metaposiciones que funciona suficientemente bien para partidas
cerca de terminar, pasando luego por un enfoque basado en búsqueda Monte Carlo y
terminando en otra técnica basada en análisis retrogrado. Favini termina con un
solver capaz de resolver juegos enteros con un desempeño aceptable.

%% Objetivos Generales
\section{Objetivos Generales}

El objetivo de este trabajo es el replicar los resultados obtenidos por Stuart 
Russell y Jason Wolfe en su trabajo \emph{Efficient belief-state AND–OR search 
with application to Kriegspiel}, particularmente con los algoritmos DFS, DBU y
DUB para búsqueda en árboles AND-OR. DFS es el clásico algoritmo de búsqueda en 
profundidad, incluido aquí para compararlo con los incrementales. DBU y DUB son
algoritmos de búsqueda incrementales. Esto quiere decir que a la hora de expandir
un estado de creencia, sólo se generan los sucesores de un estado físico a la
vez. La diferencia entre los dos consiste en el orden en que se expanden los
estados. Finalmente se desea evaluar el desempeño que tiene el uso de una
representación explícita de los estados físicos en los algoritmos, para
referencia en trabajos futuros.

%Objetivos Especificos
\section{Objetivos Específicos}

Con el fin de alcanzar los objetivos generales de este proyecto, se plantean los siguientes objetivos
específicos o subproblemas:

\begin{itemize}

\item Diseñar e implementar la representación explícita de estados físicos de
ajedrez, incluyendo el árbol de juego AND-OR-EXPAND.

\item Implementar el estimador para determinar el estado de creencia inicial de
una instancia Kriegspiel.

\item Implementar el algoritmo DFS sobre el árbol AND-OR-EXPAND.

\item Implementar el algoritmo DBU sobre el árbol AND-OR-EXPAND.

\item Implementar el algoritmo DUB sobre el árbol AND-OR-EXPAND.

\item Correr los tres algoritmos implementados sobre una base de problemas
Kriegspiel 3-ply, 5-ply y 7-ply y comparar los resultados.

\item Analizar los resultados y sacar conclusiones al respecto.

\end{itemize}
