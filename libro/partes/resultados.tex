\chapter{Resultados}
\label{Resultados}
\lhead{Capítulo 5 \emph{Resultados}}

Antes de describir los resultados obtenidos de los experimentos, es importante
dejar en claro los resultados del estimador, es decir, cómo se generaron y
validaron los estados de creencia iniciales para cada instancia Kriegspiel usada
en los experimentos.

Como ya se había descrito antes, el estimador de estados de creencia toma una
partida escrita en notación SAN de Kriegspiel, lee el historial de movimientos y
determina paso a paso cuáles estados físicos deben persistir en el estado de
creencia a lo largo del historial. Una vez que todas las jugadas son leídas y
procesadas, el estimador termina con un estado de creencia que contiene los
estados físicos posibles para ese momento en la partida.

Para validar la correctitud de tal resultado, el estimador se vale de los datos
de cabecera de cada instancia. Estos datos consisten en pares nombre-valor que
describen varias características de cada instancia. Aunque se ha proveído una
descripción completa del formato y contenido de estas líneas en el Apéndice B,
aquí se describen aquellas que hacen falta para validar el resultado del
estimador:

\begin{itemize}

\item \emph{final-states}: Describe la cantidad de estados que terminó por
contener el estado de creencia inicial al final del historial de partida. El
estimador chequea que la cantidad de estados que determinó sea la misma cuando
termina de estimar el historial.

\item \emph{white-pcs}: La cantidad de piezas que poseen las blancas al
finalizar el historial. Este número es monitoreado a medida que el estimador
hace su trabajo, y validado con esta cabecera al final.

\item \emph{black-pcs}: La cantidad de piezas que poseen las negras al finalizar
el historial. Se monitorea de manera similar a como se hace con white-pcs.

\item \emph{computed-states}: La cantidad de estados físicos generados durante
el procesamiento de la instancia. El estimador también lleva cuenta de esta
cantidad.

\end{itemize}

Luego de la validación de estos resultados, se puede pasar a correr los
algoritmos considerados en este trabajo.

Los algoritmos implementados logran satisfactoriamente resolver todas las 
instancias de Kriegspiel consideradas. La base de datos de problemas consistió
en 647 problemas de profundidad 3, 671 de profundidad 5 y 687 de profundidad 7.
Cada subset de problemas estuvo dividido por la mitad en mates y no-mates. La
diferencia entre estos dos tipos de instancia radica en que para una instancia
mate, todos los estados físicos del estado de creencia inicial admiten un mate
de Kriegspiel. Para no-mate, esto es verdad para una cantidad de estados mayor
a la mitad de los estados totales del estado de creencia inicial pero menor al
total. Mientras más estados que admitan mate existan, mayores son las
posibilidades de que el siguiente movimiento a expandir lleve a un hijo AND que
eventualmente pruebe al estado de creencia actual como verdadero. Por esto las
instancias no-mate tienden a ser las más difíciles.

Cabe destacar que la dificultad de una instancia depende tanto de su profundidad
como del hecho de ser mate o no-mate. La profundidad no hace más que describir
qué tan profundo es el árbol de búsqueda. Para una instancia de profundidad 5,
el árbol de búsqueda tiene 3 jugadas de las blancas y 2 de las negras, es decir
que además de 3 niveles OR y 2 niveles EXPAND, el árbol también posee 4 nodos
AND. La figura 3.2 es un claro ejemplo de esto. La cantidad de estados del árbol
no se puede saber a priori, y varía mucho dependiendo de los resultados de la
ejecución de los movimientos posibles de cada estado físico. Sin embargo, se
puede obtener una aproximación al mínimo de los estados físicos que pueden
haber, al recordar que cada Nodo OR tiene un hijo por cada movimiento posible,
es decir, 1 como mínimo por cada pieza blanca. Llamemos a este número
\emph{W} y a la cantidad de estados físicos iniciales \emph{N}. Entonces hay W*N
estados físicos repartidos por todos los hijos AND. Luego, cada hijo AND tiene
un nodo EXPAND con un mínimo de 1 estado físico por cada pieza negra. Llamemos a
la cantidad de piezas negras \emph{B}. Entonces tenemos que hay W*N*B estados
físicos por cada ply como mínimo, aproximadamente. Al multiplicar este número
por la profundidad de la instancia (la cantidad de jugadas de las blancas
necesarias para mate), tenemos un aproximado del mínimo de la cantidad de
estados físicos del problema en total. Entonces la dificultad de
una instancia también está determinada por la cantidad de piezas blancas y
negras al momento de iniciar la búsqueda. Estas cantidades están especificadas
en cada instancia. Más detalles sobre el formato de las instancias se encuentran
en el apéndice B. Para d3, d5 y d7, el mínimo posible de estados físicos generados al
correr los algoritmos es aproximadamente 900, 1800 y 3360 respectivamente. Hay que 
notar que este mínimo aproximado es suponiendo que cada pieza tiene tan sólo un 
movimiento en cada uno de sus turnos. En general habría que multiplicar esta 
cantidad por el número aproximado de jugadas legales que podría tener cada pieza. Este 
número varía mucho dependiendo de la instancia, pero fácilmente supera los 15 en 
general, lo que hace mucho más grande la cantidad promedio de estados de creencia 
generados. Para casos de d7, la cantidad de memoria consumida por una sola instancia
fácilmente supera los 10 Gigabytes.

En el cuadro 5.1 se tiene una variedad de datos sobre las instancias
consideradas. Se puede ver que los estados iniciales, la cantidad de piezas
blancas y negras, los estados generados en estimación y los estados que admiten
mate no difieren mucho en cantidad entre profundidades. La diferencia de
profundidad es determinada únicamente por el valor de las posiciones de tablero
presentes en el estado de creencia inicial. De particular importancia es el
tiempo promedio de corrida por instancia de cada algoritmo en cada profundidad.
A pesar de que incluso desde d3 se puede ver una mejora en los algoritmos
incrementales, es bastante ligera. Es en d5 en donde se se nota claramente la
diferencia de desempeño. Los números de d7 se deben a límites de memoria
impuestos a la hora de correr los experimentos. Esto es explicado más adelante.

% Please add the following required packages to your document preamble:
% \usepackage{booktabs}
\begin{table}[]
\centering
\begin{tabular}{@{}cccc@{}}
\toprule
\textbf{Profundidad} & \textbf{\begin{tabular}[c]{@{}c@{}}Número de \\
Estados\end{tabular}}                    &
\textbf{\begin{tabular}[c]{@{}c@{}}Número de \\ Piezas\\ Blancas\end{tabular}} &
\textbf{\begin{tabular}[c]{@{}c@{}}Número de \\ Piezas\\ Negras\end{tabular}}
\\ \midrule
d3                   & 10
& 10
& 3
\\
d5                   & 12
& 10
& 3
\\
d7                   & 12
& 10
& 4
\\ \midrule
\textbf{Profundidad} & \textbf{\begin{tabular}[c]{@{}c@{}}Estados \\ Generados
\\ en el Estimador\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Estados
que \\ Admiten\\ Mate\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Estados
que \\ Admiten \\ Mate en \\ Instancias NoMate\end{tabular}} \\ \midrule
d3                   & 7686
& 9
& 8
\\
d5                   & 7757
& 10
& 9
\\
d7                   & 7365
& 11
& 10
\\ \midrule
\textbf{Profundidad} & \textbf{\begin{tabular}[c]{@{}c@{}}Tiempo de \\ Corrida\\
DFS\end{tabular}}              & \textbf{\begin{tabular}[c]{@{}c@{}}Tiempo de \\
Corrida\\ DBU\end{tabular}}    & \textbf{\begin{tabular}[c]{@{}c@{}}Tiempo de \\
Corrida\\ DUB\end{tabular}}                             \\ \midrule
d3                   & 0.13 seg
& 0.11 seg
& 0.12 seg
\\
d5                   & 0.37 seg
& 0.14 seg
& 0.14 seg
\\
d7                   & 2 seg
& 2 seg
& 2 seg
\\ \bottomrule
\end{tabular}
\caption{Valores promedio para las instancias.}
\label{my-label}
\end{table}

Ya con las consideraciones sobre la dificultad explicadas, se puede pasar a ver
los resultados obtenidos.

\begin{figure}[h!]
\centering
\includegraphics[width=0.9\textwidth]{graphD3.png}
\caption{Resultados en el dataset de profundidad 3.}
\label{imagen:graphD3}
\end{figure}

La figura 5.1 muestra el tiempo acumulado tomado por cada algoritmo para
resolver todas las instancias de profundidad 3. Ya se puede notar una diferencia
de desempeño de los algoritmos incrementales con respecto a DFS, aunque DBU
parece ser el que tiene la corrida más rápida. Sin embargo, la diferencia no es
tan grande. Esto se debe a la poca profundidad de este dataset.

\begin{figure}[h!]
\centering
\includegraphics[width=0.9\textwidth]{graph.png}
\caption{Resultados en el dataset de profundidad 5.}
\label{imagen:graph}
\end{figure}

La figura 5.2 muestra los resultados obtenidos del dataset de profundidad 5. De
nuevo, se midió el tiempo acumulado que cada algoritmo tarda en resolver todos
los problemas. Aquí ya se nota una gran diferencia entre DFS y los algoritmos
incrementales pues el DFS tarda más del doble del tiempo total de los otros dos
algoritmos, los cuales obtienen un desempeño bastante parecido.

Las instancias de profundidad 7 también fueron probadas. Sin embargo, la gran
mayoría de las instancias sobrepasaron el límite de memoria impuesto (Para
evitar el colapso de la máquina usada en los experimentos), lo que no permite
dar un resultado preciso en lo que a las instancias de profundidad 7 se refiere.
Hay que recordar que el objetivo principal de este trabajo es replicar los
resultados de Berkeley. En aquél trabajo tampoco se reportaron resultados para
instancias de profundidad mayor a 5, pues el consumo de tiempo y memoria es
excesivo para su propósito y los resultados de profundidad 5 ya daban suficiente
información para pasar a desarrollar futuras versiones más eficientes de los
algoritmos incrementales en base a nuevas técnicas como el uso de verdaderas
tablas de transposición, ordenamiento dinámico de los movimientos y el uso de
otro tipo de representación para los estados físicos.

Al comparar los resultados en cuanto a desempeño con los obtenidos por los
mismos algoritmos en el trabajo de Berkeley expuestos en la figura 5.3 (Los
algoritmos de este trabajo corresponden con los L-DFS, L-DUB y L-DBU de la figura) 
se puede notar una mejora de desemepeño extremadamente similar a la conseguida 
en dicha investigación. Cabe destacar que la diferencia en tiempos se debe a 
que las computadoras usadas para correr los algoritmos en este trabajo son mucho 
más potentes. No obstante, las gráficas demuestran que esto no importa en lo absoluto.
Los resultados de Berkeley han sido replicados.

\begin{figure}[h!]
\centering
\includegraphics[width=1\textwidth]{graficapaper.png}
\caption{Resultados de Berkeley \cite{wolfeRussell}}.
\label{imagen:graficapaper}
\end{figure}

En la figura 5.4 se pueden ver más curvas que sólo las de los algoritmos que
implementamos. Estas curvas representan otros algoritmos que no fueron cubiertos
en esta investigación, pues el objetivo era netamente sobre DFS y los algoritmos
incrementales basados en ellos. Primero se tiene que hay variaciones de los
algoritmos cubiertos aqui, como LE-DFS y GL-DBU, por ejemplo. Cada prefijo
colocado a los algoritmos representa cambio diferente:

\begin{itemize}
\item \emph{L}: Legal-First. A la hora de tomar un nodo para expandir, el
algoritmo en cuestión tomará aquellos nodos que representen jugadas legales
primero. Esta modificación es la que contiene este trabajo.
\item \emph{G}: Greedy. Se expanden los nodos más valiosos primero en base a una
heurística. En este trabajo no se exploró agregar heurísticas porque el enfoque
era en comparar los algoritmos. Determinar y comparar heurísticas posibles para
mejorar el desempeño de los algoritmos puede incluso proveer suficiente
contenido para una investigación futura.
\item \emph{E}: Early-Termination. Esta variación sólo aplica a algoritmos
clásicos. Consiste en detener el proceso de expansión de un nodo si se consigue
algún estado físico terminal con valor falso (las negras ganaron) o algún estado
no terminal en el límite de profundidad. No se consideró implementar esta
versión por no tener homólogo para los algoritmos incrementales.
\item \emph{X}: Esta variación sólo es usada par GX-DFS, y consiste en tomar el
nodo ilegal primero a la hora de expandir.
\end{itemize}

Finalmente se tienen los algoritmos PNS, sus variaciones e IPNS. Tal como se
explicó en el capítulo 3, PNS es otro algoritmo clásico que expande primero el
mejor nodo, funcionando similar a como lo haría un algoritmo BFS. IPNS no
es más que la versión incremental de este algoritmo. A pesar de que estos
algoritmos tienen un mejor desempeño en general que DFS y sus versiones
incrementales, el trabajo de Berkeley no ahonda mucho en sus resultados. Mas
aún, comenzar por el análisis de DFS coloca las bases para su comparación con
PNS en un trabajo futuro.
