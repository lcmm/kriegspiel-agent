\chapter{Implementación}
\label{Implementacion}
\lhead{Capítulo 4. \emph{Implementación}}

\section{Representación}

La representación usada para describir los estados físicos de Kriegspiel es la
explícita, donde cada estado posee toda la información de una posición. Es
posible tener lo que se llama una representación factorizada, donde cada estado
contiene sólo un fragmento del contenido total de una posición. Las
implicaciones de usar una representación factorizada en lugar de una explícita
se dejarán para un trabajo futuro.

Un estado físico ocupa un total de 67 bytes y está compuesto por tres atributos:

\begin{itemize}

\item \emph{board}: Es un arreglo de 64 bytes que representa las piezas
actualmente en el tablero. Cada byte describe el contenido de una casilla. El
rango de posibles valores de cada byte va de -6 a 6, siendo el significado de
cada valor el siguiente:

   \begin{itemize}
   
   \item \emph{-6}: Rey Blanco
   \item \emph{-5}: Reina Blanca
   \item \emph{-4}: Torre Blanca 
   \item \emph{-3}: Caballo Blanco
   \item \emph{-2}: Alfíl Blanco
   \item \emph{-1}: Peón Blanco
   \item \emph{0}: Casilla Vacía
   \item \emph{1}: Peón Negro
   \item \emph{2}: Alfíl Negro
   \item \emph{3}: Caballo Negro
   \item \emph{4}: Torre Negra
   \item \emph{5}: Reina Negra
   \item \emph{6}: Rey Negro 
   
   \end{itemize}


\item \emph{kingData}: Es un arreglo de dos bytes. El primero es la posición
actual del rey negro y el segundo la del rey blanco. El rango de valores de cada
uno va de 0 a 63.


\item \emph{extraData}: Es un entero que va desde 0 a 255. Los primeros 4 bits
representan la posibilidad o no de enroque para las 4 esquinas del tablero. El
resto es un entero que determina si hay posibilidad de \emph{en passant}
\footnote{Jugada de captura en ajedrez donde un peón puede capturar a otro que
se haya movido dos casillas el turno anterior} en el tablero
actual. Si este entero es negativo, no hay \emph{en passant}. En caso contrario, el
valor determina en qué fila es posible hacer en passant este turno.
   
\end{itemize}

Todos estos datos son específicos para esta representación. Sin embargo, es
necesario que toda representación de kriegspiel pueda ser usada en los
algoritmos de búsqueda sin que haya necesidad de cambiar los mismos. Es por esto
que la representación explícita no es más que una implementación concreta de una
representación abstracta. Dicha representación ofrece las siguientes utilidades
a todo algoritmo que pretenda usarla:

   
   \begin{itemize}

   \item \emph{apply}: Acepta un movimiento en notación PGN y un color para
   retornar el tablero resultante de aplicar el movimiento usando las piezas de
   ese color. (el color es una constante positiva para las negras y una
   constante positiva para las blancas).


   \item \emph{generateLegalStates}: Acepta un color para retornar todos los
   estados físicos legales que pueden ser generados a partir del estado.


   \item \emph{generateMoves}: Acepta un color para retornar todos los
   movimientos posibles de las piezas de ese color en el estado. Los movimientos
   son retornados en una estructura especial independiente de los algoritmos.
   Esta estructura se le puede pasar al procedimiento apply en lugar del
   movimiento en notación PGN.

   \item \emph{checkMate}: Acepta un color y retorna si hay jaque, mate o
   ninguno en contra de las piezas de ese color en el estado.

   \item \emph{isMaterialWin}: Para las instancias de Kriegspiel de Berkeley, la
   condición de victoria de uno de los jugadores no sólo consiste en tener jaque
   mate para el rey enemigo. Esto es porque dadas ciertas condiciones del
   tablero, es posible conseguir la victoria asegurada en una cantidad finita de
   movimientos, independientemente de cómo juegue el enemigo. Este procedimiento
   determina si dichas condiciones se han cumplido para un color dado. A este
   tipo de victoria se le llama \emph{Victoria Material}. Más información sobre
   la victoria material se puede conseguir en el Apéndice A.


   \end{itemize}

\section{Estados de Creencia}

En términos generales, los estados de creencia consisten en un conjunto de
estados físicos y una tabla para evitar contener estados físicos con las mismas
piezas en las mismas posiciones. En nuestra implementación, los estados de creencia 
posseen dos atributos:

\begin{itemize}

\item \emph{states}: Es un vector con los estados físicos actuales del estado de
creencia.

\item \emph{transpositionTable}: Es una tabla que guarda las claves Zobrist de
cada estado físico para detectar duplicados. Una clave Zobrist consiste en un
número de 768 bits que depende del contenido de cada casilla del tablero, la
posibilidad de hacer en passant para cada fila y la posibilidad para hacer
enroque de cada esquina. Cuando un estado físico nuevo entra al estado de
creencia, se guarda en esta tabla usando su clave Zobrist. Pueden existir dos
estados físicos con la misma clave Zobrist. En caso de querer agregar un estado
duplicado, se cancela la operación.

\end{itemize}

\section{Nodos}

Los nodos del árbol AND-OR-EXPAND usan la estructura del estado de creencia para
mantener sus estados físicos. Además de esto, poseen los siguientes atributos:

\begin{itemize}

\item \emph{value}: El valor actual de probado o no del nodo

\item \emph{terminal}: Valor que determina si el nodo es terminal o no

\item \emph{depth}: La profundidad del nodo en el árbol actual. Una profundidad
de 1 indica nodo hoja

\item \emph{children}: Un vector con los hijos de este nodo

\end{itemize}

\section{Estimador}

La implementación del estimador es primeramente apoyada por un procedimiento
llamado \emph{readProblemFile}, el cual lee un archivo en notación PGN de
Kriegspiel y retorna una lista de pares de jugadas correspondientes a las
descritas en el archivo. Estas jugadas son usadas como la entrada al método
principal del estimador: \emph{estimate}, el cual acepta una lista de pares de
jugadas y un estado de creencia base (conteniendo un sólo estado físico con el
estado inicial del tablero) para retornar el estado de creencia inicial para la
instancia.

\begin{figure}
\centering
\label{imagen:estimate}
\begin{BVerbatim}
void estimate(List of turns play, Node bs)
   for(Turn turn in play)
      for(State s in bs.states)
	 if(cualquiera de las jugadas ilegales blancas son aplicables a s)
	    bs.states.remove(s)
      for(State s in bs.states)
	 if(la jugada legal blanca no puede ser aplicada a s)
	    bs.states.remove(s)
	    continue
	 c = APPLYMOVE(white legal move, s)
	 if(el estado c se contradice con el percepto del juez)
	    bs.states.remove(s)
	 if(la cantidad de posibles jugadas ilegales en c son menores a las
	 especificadas por el juez)
	    bs.states.remove(s)
	 for(State b in GenerateLegalStates(c,BLACK))
	    if(El estado de b se contradice con el percepto del juez)
	       continue
	    bs.states.push(b)
	 bs.states.remove(s)
\end{BVerbatim}
\caption{Pseudocódigo de estimate.}
\end{figure}

\emph{estimate} funciona de la siguiente manera: Por cada par de jugadas,
comienza procesando la primera (la jugada de las blancas), la cual siempre
estará completamente definida. Primero, por cada estado actualmente en el estado
de creencia, intenta aplicar los movimientos que las blancas intentaron pero
resultaron ilegales. Cualquier estado físico que logre aplicar alguno de esos
movimientos es eliminado del estado de creencia. Luego, intenta aplicar los
movimientos legales, eliminando todos aquellos estados que no logren aplicar el
movimiento. Los que si apliquen el movimiento terminan siendo el nuevo estado de
creencia actual. Para todos estos estados, se chequea si son consistentes con el
percepto del juez, eliminando a los que no lo sean. De esta manera se termina de
procesar la primera jugada para pasar a procesar la segunda. Primero se
verifica si la cantidad de movimientos ilegales en cada estado físico es menor a
la cantidad de intentos ilegales de las negras reportadas por el juez. Se
eliminan todos los estados que cumplan con esta condición. Finalmente, para cada
estado físico del estado de creencia actual, se generan todos sus posibles
movimientos negros y se agregan al estado de creencia si son legales y
consistentes con el percepto del juez. Todo el proceso se repite para el resto
de los pares de jugadas.

Ya con el estado de creencia inicial, se puede pasar a implementar los
algoritmos de búsqueda.

\section{DFS}

Lo primero que se implementó del DFS en este trabajo fue el método EXPAND. La
implementación fue, en mayor parte, bastante fiel al pseudo-código explicado en
el capítulo 3. Sin embargo, hay una buena cantidad de detalles en el desarrollo
que se escapan de la teoría para mantener la eficiencia a la hora de hacer la
búsqueda.

Para la instancia OR de EXPAND, se usó el método \emph{generateMoves} en el
primer estado físico del nodo y se aplicó a ese y todos los demás estados. Un
cambio notorio entre el pseudo-código y esta implementación es que para aquellos
estados que resulten ilegales ocurre que son marcados como tal y agregados al
hijo AND de ese movimiento de esa forma. Esto es para evitar ramas de
movimientos ilegales infinitas, ya que si todos los estados para un movimiento
dado resultan ilegales, ese hijo es eliminado. Luego de generar los estados en
cada hijo, para cada uno de ellos se ve si representan una victoria para las
blancas y se eliminan los estados físicos que cumplan con esa condición. El nodo
se marca como terminal si no posee estados físicos.

Para la instancia AND se dividieron los hijos en tres perceptos potenciales:
ilegales, normales y capturas. Esto porque la ganancia de eficiencia de dividir
en mayor cantidad de perceptos es mucho menor en comparación con alguna otra técnica, 
por lo que no fue visto como necesario. El nodo ilegal siempre es OR, y sólo es
generado si hay algún estado físico en el estado de creencia que sea ilegal. Los
otros dos hijos son OR o EXPAND dependiendo de que tipo de nodo fue el padre, lo
cual es determinado en la implementación con el color del jugador actual. Si el
jugador es las blancas, los hijos son EXPAND. Si el jugador es las negras, los
hijos son OR. Estos hijos son divididos entre capturas y no capturas. Todos los
hijos (excepto el ilegal) tienen una profundidad mayor que el padre.

Para la instancia EXPAND, se creo un sólo nodo AND hijo de la misma profundidad.
Por cada estado físico en el estado de creencia se generan todos sus estados
legales y se agregan al estado de creencia del hijo. Si alguno de esos estados
es una victoria de las negras, el hijo es terminal con valor falso.

Ya con el EXPAND implementado se pasa al DFS, primero con SOLVE-TOP, el cual
consiste simplemente en llamar a la instancia de SOLVE del nodo actual.

La instancia de SOLVE para nodos OR consiste primero en expandir los hijos del
nodo con EXPAND, y luego correr SOLVE-TOP en cada uno de los hijos hasta que
alguno sea probado. Si es así, el nodo OR es probado. Si ningún hijo es probado,
el nodo queda como refutado.

La instancia de SOLVE para nodos AND retorna su valor actual si es terminal. En
caso contrario se expande el nodo y se llama a SOLVE-TOP para cada hijo. Si alguno 
es refutado, el nodo AND es refutado. Si ninguno es refutado, el nodo es probado.

La instancia de SOLVE para los nodos EXPAND se trata simplemente de expandir el
nodo y luego retornar el valor de SOLVE-TOP en su único hijo.

\section{DBU}

Como en fue descrito en teoría, para implementar los algoritmos incrementales se
necesita desarrollar primero el INCREMENTAL-EXPAND para poder generar los hijos
de los nodos incrementalmente uno por uno. Hay que recordar que a éste método se
le pasa un estado físico del estado de creencia diferente en cada llamada.

La instancia OR de este método verifica primero si el nodo actual tiene hijos.
Si no los tiene, genera un hijo AND vacío de la misma profundidad para cada posible 
movimiento del estado físico pasado como argumento. Luego (independientemente de
que el nodo haya tenido hijos o no), se aplica el movimiento asociado a cada
hijo al estado físico pasado y se agrega el resultado al hijo correspondiente si
y sólo si se cumplen las siguientes condiciones:

\begin{itemize}

\item El estado resultante es legal.
\item El estado resultante no es terminal.
\item No nos encontramos al límite de profundidad.
\item No hay victoria real o material para las blancas en el estado resultante.

\end{itemize}

Si alguna de las últimas tres condiciones es verdadera, entonces el hijo será
eliminado del nodo actual si se cumple que hay una victoria real o material para
las blancas. En caso que no, simplemente no se agrega el estado físico al hijo
actual.

Si se llega a agregar el estado físico al nodo hijo, se marca también al nodo
actual como no terminal.

La instancia de INCREMENTAL-EXPAND para nodos AND también revisa si el nodo no
tiene hijos. En ese caso se crean dos nodos EXPAND (Si el nodo padre es OR) o
dos nodos OR (si el nodo padre es EXPAND). Estos dos hijos representan el
percepto de captura y no captura. Luego se agrega el estado físico pasado como
argumento a alguno de los dos hijos dependiendo de si había captura o no.

Finalmente se tiene la instancia de este método para nodos EXPAND, la cual
igualmente ve si el nodo no tiene hijos y en caso positivo crea un nodo AND y lo
coloca como su hijo. Luego, para cada movimiento posible de las negras en el
estado pasado, se agrega el estado al hijo. Si llega a pasar que alguno de los
estados físicos es terminal, se coloca al hijo como terminal con valor falso y
se termina la creación de estados de manera temprana.

Ya con INCREMENTAL-EXPAND terminado se procede a implementar DBU, comenzando por
su método tope SOLVE-TOP. SOLVE-TOP consiste en un ciclo. Mientras el estado de 
creencia no esté vacío, sacar el primer estado físico del nodo y llamar a 
INCREMENTAL-EXPAND con el mismo. Luego, si SOLVE retorna falso, el nodo es refutado. 
Si se logra salir del ciclo, el nodo es probado. Hay que notar que las instancias de
SOLVE mencionadas son las mismas que para DFS, solo que las llamadas internas a
SOLVE-TOP son las del DBU.

\section{DUB}

DUB tambíen hace uso de las instancias de INCREMENTAL-EXPAND y SOLVE que se han
desarrollado. Primero se implementa OUTER-TOP (El método tope o de entrada), el
cual no es más que llamar a SOLVE-TOP (Idéntico al de DBU salvo por una
diferencia explicada más adelante) y a un nuevo método llamado OUTER. Si ambas
llamadas retornan verdadero, el nodo es probado.

La diferencia del SOLVE-TOP de DUB en comparación con DBU es que la instancia
SOLVE de AND sólo llama a SOLVE-TOP de uno de sus hijos (en caso de no ser
terminal).

OUTER tiene una instancia para cada tipo de nodo. Para nodos OR, OUTER es un
ciclo que continua mientras hayan hijos en el nodo. En cada iteración, se llama
OUTER del primer hijo. Si retorna verdadero, OUTER devuelve verdadero. En caso
contrario se elimina al hijo y se llama a SOLVE. Si SOLVE retorna falso, OUTER
retorna falso. Si el ciclo entero pasa y no se ha terminado el método, se
retorna falso.

La instancia AND retorna el valor del nodo si es terminal. En caso contrario es
similar al OUTER de OR, pues llama a OUTER por cada hijo, sólo que si resulta
falso, devuelve falso. Si es verdadero, el hijo es eliminado, y si no quedan más
hijos en ese momento se devuelve verdadero. Si aun no se devuelve un resultado,
se llama a SOLVE. Si éste es falso se retorna falso. Si no, se repite el ciclo.

Como siempre, la instancia del nodo EXPAND es la más simple. Si no hay hijos,
retorna falso. En caso contrario retorna el valor de OUTER en su hijo.
