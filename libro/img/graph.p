set title "Algoritmos"
set xlabel "Tiempo"
set ylabel "Porcentaje"
set autoscale
set ytics 0,0.25
set key right bottom
plot "graphDFS.txt" using 2:1 title 'DFS' with lines, \
     "graphDBU.txt" using 2:1 title 'DBU' with lines, \
     "graphDUB.txt" using 2:1 title 'DUB' with lines
set term png size 720,720
set output "graph.png"
replot
set term x11
