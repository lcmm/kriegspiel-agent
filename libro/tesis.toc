\select@language {spanish}
\contentsline {chapter}{Resumen}{\es@scroman {i}}{dummy.1}
\vspace {1em}
\contentsline {chapter}{Agradecimientos}{\es@scroman {ii}}{dummy.2}
\vspace {1em}
\contentsline {chapter}{\IeC {\'I}ndice de Figuras}{\es@scroman {v}}{dummy.4}
\contentsline {chapter}{Acr\IeC {\'o}nimos y S\IeC {\'\i }mbolos}{\es@scroman {vi}}{dummy.6}
\vspace {2em}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.9}
\contentsline {chapter}{Introducci\IeC {\'o}n}{1}{chapter.9}
\contentsline {section}{\numberline {1.1}Antecedentes}{3}{section.10}
\contentsline {section}{\numberline {1.2}Objetivos Generales}{4}{section.11}
\contentsline {section}{\numberline {1.3}Objetivos Espec\IeC {\'\i }ficos}{5}{section.12}
\contentsline {chapter}{\numberline {2}Marco Te\IeC {\'o}rico}{6}{chapter.13}
\contentsline {section}{\numberline {2.1}Juegos suma cero con informaci\IeC {\'o}n completa}{6}{section.14}
\contentsline {section}{\numberline {2.2}Representaci\IeC {\'o}n y Algoritmos Cl\IeC {\'a}sicos para juegos con informaci\IeC {\'o}n completa}{7}{section.15}
\contentsline {section}{\numberline {2.3}Juegos con informaci\IeC {\'o}n incompleta}{9}{section.17}
\contentsline {section}{\numberline {2.4}Representaci\IeC {\'o}n y Algoritmos Cl\IeC {\'a}sicos para juegos con informaci\IeC {\'o}n incompleta}{10}{section.18}
\contentsline {section}{\numberline {2.5}Kriegspiel}{11}{section.19}
\contentsline {chapter}{\numberline {3}Planteamiento del Problema}{14}{chapter.20}
\contentsline {section}{\numberline {3.1}Estructuras}{14}{section.21}
\contentsline {section}{\numberline {3.2}Algoritmos Cl\IeC {\'a}sicos}{16}{section.24}
\contentsline {section}{\numberline {3.3}Algoritmos Incrementales}{20}{section.27}
\contentsline {section}{\numberline {3.4}Requerimientos Previos}{23}{section.30}
\contentsline {chapter}{\numberline {4}Implementaci\IeC {\'o}n}{25}{chapter.31}
\contentsline {section}{\numberline {4.1}Representaci\IeC {\'o}n}{25}{section.32}
\contentsline {section}{\numberline {4.2}Estados de Creencia}{27}{section.34}
\contentsline {section}{\numberline {4.3}Nodos}{28}{section.35}
\contentsline {section}{\numberline {4.4}Estimador}{28}{section.36}
\contentsline {section}{\numberline {4.5}DFS}{29}{section.38}
\contentsline {section}{\numberline {4.6}DBU}{31}{section.39}
\contentsline {section}{\numberline {4.7}DUB}{32}{section.40}
\contentsline {chapter}{\numberline {5}Resultados}{34}{chapter.41}
\contentsline {chapter}{\numberline {6}Conclusiones y Recomendaciones}{42}{chapter.46}
\vspace {2em}
\contentsline {chapter}{\numberline {A}Reglas de Kriegspiel}{44}{appendix.48}
\contentsline {section}{\numberline {A.1}Movimiento de Piezas}{44}{section.49}
\contentsline {section}{\numberline {A.2}El Juez}{45}{section.50}
\contentsline {subsection}{\numberline {A.2.1}Legalidad}{45}{subsection.51}
\contentsline {subsection}{\numberline {A.2.2}Capturas}{45}{subsection.52}
\contentsline {subsection}{\numberline {A.2.3}Jaques}{45}{subsection.53}
\contentsline {subsection}{\numberline {A.2.4}Tablas y Jaque Mate}{46}{subsection.54}
\contentsline {section}{\numberline {A.3}Condiciones de Tablas y Jaque Mate}{46}{section.55}
\contentsline {subsection}{\numberline {A.3.1}Tablas}{46}{subsection.56}
\contentsline {subsection}{\numberline {A.3.2}Tablas Materiales}{46}{subsection.57}
\contentsline {subsection}{\numberline {A.3.3}Jaque Mate Material}{47}{subsection.58}
\contentsline {chapter}{\numberline {B}Notaci\IeC {\'o}n PGN}{48}{appendix.59}
\contentsline {section}{\numberline {B.1}Cabecera}{48}{section.60}
\contentsline {section}{\numberline {B.2}Movimientos}{49}{section.61}
\contentsline {section}{\numberline {B.3}Kriegspiel}{49}{section.62}
\vspace {2em}
