#include "Nodo.h"
#include <iostream>
#include <iterator>
#include <algorithm>

void Node::prnt(){
   cout << " value: " << this->value << " terminal: " << this->terminal << " depth: " << this->depth<<endl;
   beliefState.print(this->depth-1);
};

void Node::markForRemoval(){
   toRemove = true;
}

bool isReadyForRemoval(Node* n){
   return n->toRemove;
}

void Node::clean(vector<Node*>& v){
   v.erase(remove_if(v.begin(),v.end(),isReadyForRemoval),v.end());
}

void Node::printTabs(int tabs){
   for(int i=0;i<tabs;++i){
      cout << "  ";
   }
}

void Node_And::print(){
   cout << "Nodo_And:";
   this->prnt();
};

void Node_Or::print(){
   cout << "Nodo_Or:";
   this->prnt();
};

void Node_Expand::print(){
   cout << "Nodo_Expand:";
   this->prnt();
};

void Node_Or::expand(){
   ChessState* ans = this->beliefState.states[0];

   for(moveResult mr : ans->generateMoves(WHITE)){
      Move m = mr.move;
      Node_And* child = new Node_And(depth,WHITE,m);
      for(ChessState* state : beliefState.states){
	 ChessState* next = state->applyMove(m,WHITE,true);
	 if(!next){
	    continue;  
	 }else{
	    if(m.capture) next->capture = true;
	    next->legal = true;
	 }
	 child->beliefState.addState(next);
      }
      if(child->beliefState.empty()){
	 delete(child);
	 child = nullptr;
	 continue;
      }
      for(ChessState* state : child->beliefState.states){
	 if(state->winner == WHITE || state->isMaterialWin(WHITE)){
	    state->markForRemoval();
	 }else if(state->terminal || depth == 1){
	    delete(child);
	    child = nullptr;
	    break;
	 }
      }
      if(child){
	 child->beliefState.clean();
	 children.push_back(child);
	 if(child->beliefState.states.size() == 0){
	    child->terminal = true;
	    break;
	 }
      }
   }
}

void Node_And::expand(){
   Node_Or* illegalChild = new Node_Or();
   illegalChild->depth = depth;

   if(player == WHITE){
      Node_Expand* captureChild = new Node_Expand(depth-1,move);
      Node_Expand* normalChild = new Node_Expand(depth-1,move);
      for(ChessState* n : beliefState.states){
	 if(n->legal){
	    if(n->capture){
	       captureChild->beliefState.addState(n);
	    }else{
	       normalChild->beliefState.addState(n);
	    }
	 }else{
	    illegalChild->beliefState.addState(n);
	 }
      }
      if(!captureChild->beliefState.empty()){
	 children.push_back(captureChild);
      }
      if(!normalChild->beliefState.empty()){
	 children.push_back(normalChild);
      }
   }else if(player == BLACK){
      Node_Or* captureChild = new Node_Or(depth-1);
      Node_Or* normalChild = new Node_Or(depth-1);
      for(ChessState* n : beliefState.states){
	 if(n->capture){
	    captureChild->beliefState.addState(n);
         }else{
            normalChild->beliefState.addState(n);
         }
      }
      if(!captureChild->beliefState.empty()){
	 children.push_back(captureChild);
      }
      if(!normalChild->beliefState.empty()){
	 children.push_back(normalChild);
      }
   }

   //Illegal child is always pushed last
   if(!illegalChild->beliefState.empty()){
      children.push_back(illegalChild);
   }
}

void Node_Expand::expand(){
   Node_And* child = new Node_And(depth,BLACK,move);
   
   for(ChessState* state : beliefState.states){
      for(moveResult m : state->generateLegalStates(BLACK)){
	 if(m.move.capture) m.state->capture = true;
	 child->beliefState.addState(m.state);
      }
   }

   child->value = false;
   child->terminal = false;
   for(ChessState* state : child->beliefState.states){
      if(state->terminal && state->winner != WHITE){
	 child->terminal = true;
	 break;
      }
   }

   children.push_back(child);
}

//**********************************DFS******************************************

bool Node::solve_top_dfs(bool legalFirst){
   expand();
   return solve_dfs(legalFirst);
}
 
bool Node_Or::solve_dfs(bool legalFirst){
   while(!children.empty()){
      if(children[children.size()-1]->solve_top_dfs(legalFirst)){
         return true;
      }
      children.pop_back();
   }
   return false;
}

bool Node_And::solve_dfs(bool legalFirst){
   if(terminal){
      return value;
   }
   while(!children.empty()){
      if (!children[children.size()-1]->solve_top_dfs(legalFirst)){
         return false;
      }
      children.pop_back();
   }
   return true;
}

bool Node_Expand::solve_dfs(bool legalFirst){
   return children[0]->solve_top_dfs(legalFirst);
}

//*****************************Incremental**********************************************

void Node_Or::incremental_expand(ChessState* s){
   if(children.empty()){
      vector<moveResult> moves = s->generateMoves(WHITE);
      for(moveResult mr : moves){
	 Node_And* child = new Node_And(depth,WHITE,mr.move,true);
	 children.push_back(child);
      }
   }

   for(Node* child : children){
      ChessState* next = s->applyMove(child->move,WHITE,true);
      if(!next)continue;
      bool materialWhite = next->isMaterialWin(WHITE);
      if(next->terminal || depth == 1 || materialWhite){
	 if(!(next->winner == WHITE) || !materialWhite){
	    child->markForRemoval();
	 }
      }else{
	 child->beliefState.addState(next);
	 child->terminal = false;
      }
   }
   clean(children);
}

void Node_And::incremental_expand(ChessState* s){
   if(children.empty()){
      if(player == WHITE){
	 Node_Expand* captureChild = new Node_Expand(depth-1,move);
	 children.push_back(captureChild);
	 Node_Expand* normalChild = new Node_Expand(depth-1,move);
	 children.push_back(captureChild);
      }else if(player == BLACK){
	 Node_Or* captureChild = new Node_Or(depth-1);
	 children.push_back(captureChild);
	 Node_Or* normalChild = new Node_Or(depth-1);
	 children.push_back(normalChild);
      }
   }

   if(s->capture) children[0]->beliefState.addState(s);
   else children[1]->beliefState.addState(s);
}

void Node_Expand::incremental_expand(ChessState* s){
   if(children.empty()){
      children.push_back(new Node_And(depth,BLACK,move,false));
      children[0]->value = false;
   }

   for(moveResult mr : s->generateLegalStates(BLACK)){
      if(mr.state->terminal){
	 children[0]->terminal = true;
	 break;
      }else{
	 children[0]->beliefState.addState(mr.state);
      }
   }
}


//***************************DBU***************************************

bool Node::solve_top_dbu(){
   while(!beliefState.states.empty()){
      incremental_expand(beliefState.pop());
      if(!solve_dbu()) return false;
   }
   return true;
}

bool Node_Or::solve_dbu(){
   for(Node* n : children){
      if(n->hasLegal){
	 if(n->solve_top_dbu()) return true;
      }
   }
   return false;
}

bool Node_And::solve_dbu(){
   if(terminal) return value;
   for(Node* n : children){
      if(!n->solve_top_dbu()) return false;
   }
   return true;
}

bool Node_Expand::solve_dbu(){
   return children[0]->solve_top_dbu();
}

//*******************DUB*********************************

bool Node::outer_top_dub(){
   return solve_top_dub() && outer_dub();
}

bool Node::solve_top_dub(){
   while(!beliefState.states.empty()){
      incremental_expand(beliefState.pop());
      if(!solve_dub()) return false;
   }
   return true;
}

bool Node_Or::outer_dub(){
   while(!children.empty()){
      if(children[children.size()-1]->outer_dub()) return true;
      children.pop_back();
      if (!solve_dub()) return false;
   }

   return false;
}

bool Node_And::outer_dub(){
   if(terminal) return value;
   while(true){
      if(!children[children.size()-1]->outer_dub()) return false;
      children.pop_back();
      if(children.empty()) return true;
      if (!solve_dub()) return false;
   }
   return false;
}

bool Node_Expand::outer_dub(){
   if(children.size() == 0) return false;
   return children[0]->outer_dub();
}

bool Node_Or::solve_dub(){
   for(Node* n : children){
      if(!n->solve_top_dub()) return false;
   }
   return true;
}

bool Node_And::solve_dub(){  
   if(terminal) return value;
   return children[1]->solve_top_dub();
}

bool Node_Expand::solve_dub(){
   return children[0]->solve_top_dub();
}
