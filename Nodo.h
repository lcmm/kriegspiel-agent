#ifndef NODO_H
#define NODO_H

#include "beliefState/ChessState.h"
#include "beliefState/BeliefState.h"
#include <set>

class Node {
public:
   BeliefState beliefState;
   Move move;
   bool value, terminal,toRemove = false;
   bool hasLegal;
   unsigned int depth;
   vector<Node*> children;

   virtual void expand() = 0;
   virtual void incremental_expand(ChessState* s) = 0;
   void prnt();
   virtual void print() = 0;
   void printTabs(int tabs);

   bool solve_top_dfs(bool);
   bool solve_top_dbu();
   bool solve_top_dub();
   bool outer_top_dub();
   virtual bool solve_dfs(bool) = 0;
   virtual bool solve_dbu() = 0;
   virtual bool solve_dub() = 0;
   virtual bool outer_dub() = 0;

   void markForRemoval();
   void clean(vector<Node*>& v);
};

class Node_And : public Node{
public:
   int player;
   Node_And(int d, int p){ value = true; terminal = false, depth = d; player = p; };
   Node_And(int d, int p, Move m){ value = true; terminal = false; depth = d; player = p;move = m; };
   Node_And(int d, int p, Move m, bool t){ value = true; terminal = t; depth = d; player = p;move = m; };

   virtual void print();
   virtual void expand();
   virtual void incremental_expand(ChessState* s);
   virtual bool solve_dfs(bool);
   virtual bool solve_dbu();
   virtual bool solve_dub();
   virtual bool outer_dub();

};

class Node_Or : public Node{
public:
   Node_Or(){ value = true; terminal = false; depth = 0; };
   Node_Or(int d) { depth = d; value = true; terminal = false;};

   virtual void print();
   virtual void expand();
   virtual void incremental_expand(ChessState* s);
   virtual bool solve_dfs(bool);
   virtual bool solve_dbu();
   virtual bool solve_dub();
   virtual bool outer_dub();
};

class Node_Expand : public Node{
public:
   Node_Expand(){ value = true; terminal = false; depth = 0; };
   Node_Expand(int d, Move m) {value = true; terminal = false; depth = d; move = m; };

   virtual void print();
   virtual void expand();
   virtual void incremental_expand(ChessState* s);
   virtual bool solve_dfs(bool);
   virtual bool solve_dbu();
   virtual bool solve_dub();
   virtual bool outer_dub();
};

#endif
