#!/usr/bin/env python

import os
from os.path import isfile, join
import subprocess
import time
import signal

command = "./dfs.out "
depth = " 3"
FNULL = open(os.devnull, 'w')
path = "./d3"
tacum = 0
timeout = 1

class Alarm(Exception):
    pass

def alarm_handler(signum, frame):
    raise Alarm

signal.signal(signal.SIGALRM, alarm_handler)

for file in os.listdir(path):
    check = True
    for line in open(join(path,file)):
        if "O-O-O" in line:
            check = False
            break
        if "O-O" in line:
            check = False
            break
        if "=" in line:
            check = False
            break

    if check:
        cmd = command + join(path,file) + depth
        #        print cmd
        signal.alarm(1)
        start = time.clock()
        try:
            process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
            signal.alarm(0)
        except Alarm:
            print "TIMEOUT\t" + file
            
        output = process.communicate()[0]        
        end = time.clock()
        if (output.split('\n')[-2])=="Solved!" :
            print file + "\t",
            tacum += end - start
            print str(tacum) 
        else:
            print (output.split('\n')[-2]) + "\t " + file
