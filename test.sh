#! /bin/bash

Verbose=false
DirDefault=./d7
fail=0
success=0
zero=0
ErrorDefault=errorD7.txt
ResDefault=resD7.txt

> t.txt
> $ResDefault

while getopts "vd:f:" opt
do
    case $opt in
	v ) Verbose=true ;;
	d ) Dir=$OPTARG ;;
	* ) echo '(╯°□°）╯︵ ┻━┻' 
	    exit
    esac
done

if $Verbose 
then
    make clean
    make
else
    printf 'compilando...'
    make clean > /dev/null
    make > /dev/null
    printf 'done!\n'
fi

echo ${Dir:-$DirDefault}
#for file in $(ls ${Dir:-$DirDefault})
for file in $(grep -L -E "(O-O-O|O-O|=)" ${Dir:-$DirDefault}/*)
do
    #    printf "." 1>&2
#    echo $file
#    answer=$(./estimador.out ${Dir:-$DirDefault}/$file | tail -n1)
#    result=$(grep "final-states" ${Dir:-$DirDefault}/$file | awk -F" \"" '{print $2}' | awk -F"\"" '{print $1}')
    answer=$(./dfs.out $file | tail -n1)
    result=$(grep "final-states" $file | awk -F" \"" '{print $2}' | awk -F"\"" '{print $1}')

    printf $file    
    if [ $answer != $result ]
    then
	printf " : "
	printf $result
	printf " = "
	printf $answer
	fail=$(($fail+1))
	if [ $answer == 0 ]
	then
	    zero=$(($zero+1))
	    echo $file >> $ErrorDefault
	fi
    else
	success=$(($success+1))
	echo $file >> $ResDefault
    fi
    
    if $Verbose 
    then
	printf "\n"
	read -p "Enter para continuar" reply
    fi
    printf "\n"

done
echo "done!" #1>&2
echo "fail = " $fail " success = " $success " zero = " $zero
