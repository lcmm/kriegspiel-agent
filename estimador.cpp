#include "estimador.h"

using namespace std;

ResponseInfo generateInfo(string j, int color){
   ResponseInfo ans;
   int cur = 2;

   //percept
   if(j[cur] == 'X'){
      string cap(j,cur,3);
      ans.percept = cap;
      cur += 3;
      if(j[cur] == ',') ++cur;
   }else{
      ans.percept = "";
   }

   //checklist
   if(j[cur] == 'C'){
      string ch;
      while(j[cur] != ':'){
	 ch.push_back(j[cur]);
	 ++cur;
      }
      ans.checkList = ch;
   }else{
      ans.checkList = "";
   }

   ++cur;

   //illegalMoves
   if(color == -1){
      while(j[cur] != ')'){
	 string m;
	 while(j[cur] != ',' && j[cur] != ')'){
	    m.push_back(j[cur]);
	    ++cur;
	 }
	 if(j[cur] == ',') ++cur;
	 ans.illegalMoves.push_back(m);
      }
   }

   //illegalSize
   if(color == 1){
      string n;
      while(j[cur] != ')'){
	 n.push_back(j[cur]);
	 ++cur;
      }
      ans.illegalSize = atoi(n.c_str());
   }

   return ans;

}

playset readProblemFile(string filename){
   string line;
   ifstream infile(filename);
   playset ans;

   for(int i=0;i<HEADER_LENGTH;++i){
      getline(infile,line);
   }

   getline(infile,line);
   int color = -1;

   turn t;

   while(getline(infile,line)){
      istringstream iss(line);
      Play p;
      
      string legal;
      string judge;

      if(color == -1){
	 string no;
	 if(!(iss >> no >> legal >> judge)){
	    cout << "Error reading white" << endl;
	    break;
	 }
      }else{
	 if(!(iss >> legal >> judge)){
	    cout << "Error reading black" << endl;
	    break;
	 }
      }
      
      p.legalMove = legal;
      p.color = color;
      p.responseInfo = generateInfo(judge,color);

      if(color == -1) t.first = p;
      else{
	 t.second = p;
	 ans.push_back(t);
      }

      color = -color;
   }

   return ans;
}


void printPlayset(playset p){
   cout << "----------------------------------" << endl;
   cout << endl;
   cout << "Playset size: " << p.size() << endl;
   int i=0;
   for(turn t : p){
      cout << "Turno numero " << ++i << endl;
      cout << "  Blancas: " << endl;
      cout << "    Legal Move: " << t.first.legalMove << endl;
      cout << "    Percept: " << t.first.responseInfo.percept << endl;
      cout << "    CheckList: " << t.first.responseInfo.checkList << endl;
      cout << "    Illegal Moves: ";
      for(string a : t.first.responseInfo.illegalMoves){
	 cout << a << ",";
      }
      cout << endl;
      cout << "  Negras: " << endl;
      cout << "    Legal Move: " << t.second.legalMove << endl;
      cout << "    Percept: " << t.second.responseInfo.percept << endl;
      cout << "    CheckList: " << t.second.responseInfo.checkList << endl;
      cout << "    IllegalSize: " << t.second.responseInfo.illegalSize << endl;
   }
   cout << endl;
   cout << "----------------------------------" << endl;
}

string getPos(string percept){
   string res = "";
   res.push_back(percept[percept.size()-2]);
   res.push_back(percept[percept.size()-1]);
   return res;
}

void estimate(playset p, BeliefState* bs){
   int turnNo = 0;

   for(turn pl : p){
      //cout << "Turn number " << ++turnNo << endl;
      
      for(string im : pl.first.responseInfo.illegalMoves){
	 for(ChessState* s : bs->states){
	    ChessState* c = s->apply(im,pl.first.color, true);
	    if(c){ 
	       s->markForRemoval();
	       delete(c);
	    }
	 }
      }
      
      bs->clean();
      vector<ChessState*> whiteLegal;
      vector<ChessState*> toAdd;

      for(ChessState* s : bs->states){
	 string to;
	 Move m = s->genMove(pl.first.legalMove,pl.first.color,&to);
	 ChessState* c = (ChessState*) s->applyMove(m, pl.first.color);
	 if(!c){
	    s->markForRemoval();
	    continue;
	 }
	 if(pl.first.responseInfo.checkList != ""){
	    if(!c->checkMate(BLACK , pl.first.responseInfo.checkList)){
	       s->markForRemoval();
	       delete(c);
	       continue;
	    }
	 }else{
	    if(c->checkMate(BLACK)){
	       s->markForRemoval();
	       delete(c);
	       continue;
	   }
	}

	 if((pl.first.responseInfo.percept != "" && !m.capture)||
	    (pl.first.responseInfo.percept == "" && m.capture)){
	      s->markForRemoval();
	      delete(c);
	      continue;
	 }

	 if(pl.first.responseInfo.percept != "" && m.capture){
	    string p = getPos(pl.first.responseInfo.percept);
	    if(p!=to){
	       s->markForRemoval();
	       delete(c);
	       continue;
	    }  
	 }
	 	
	 vector<moveResult> mr = c->generateLegalStates(BLACK);
	 if(c->illegal < pl.second.responseInfo.illegalSize){
	    delete(c);
	    s->markForRemoval();
	    continue;
	 }

	 whiteLegal.push_back(c);
	 for(moveResult t : mr){
	    
	    bool mustAdd = true;
	    if(pl.second.responseInfo.checkList != ""){
	       mustAdd = t.state->checkMate(WHITE);
	    }else{
	       mustAdd = !t.state->checkMate(WHITE);
	    }

	    if(mustAdd && pl.second.responseInfo.percept != ""){
	       if(t.capture){
		  string p = getPos(pl.second.responseInfo.percept);
		  mustAdd = (p == t.to);
	       }else{
		  mustAdd = false;
	       }
	    }else if(mustAdd && pl.second.responseInfo.percept == ""){
	       if(t.capture){
		  mustAdd = false;
	       }	       
	    }
	    
	    if(mustAdd) toAdd.push_back(t.state);
	    
	 }
	 s->markForRemoval();
      }

      if(whiteLegal.size() == 0){
	 cout << "INVALID PROBLEM" << endl;
      }
      bs->clean();
      bs->addStates(toAdd);  
   }

   bs->print();
}

/*int main(int argc, char** argv){
   playset p(readProblemFile(argv[1]));
   //printPlayset(p);
   ChessState::initSeed();
   Cstate s;
   BeliefState bs(10000000);
   bs.addState(&s);
   estimate(p,&bs);
   return 0;
}*/
