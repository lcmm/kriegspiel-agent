#ifndef C_STATE_H
#define C_STATE_H

#include "beliefState/ChessState.h"
#include <set>
#include <iomanip>


#define P   1
#define N   3
#define B   2
#define R   4
#define Q   5
#define K   6

#define RIGHT 1
#define LEFT 2
#define UP 3
#define DOWN 4


//true = row, false= column
typedef pair<bool,int> Disambiguation;

class Cstate : public ChessState{
private:
   int getPiece(int piece, int color);
   int getPieceColor(int piece);
   int kingMove(vector<Move>* ans, int color, bool enemy);
   int queenMove(vector<Move>* ans, int color, bool enemy);
   int rookMove(vector<Move>* ans, int color, bool enemy);
   int bishopMove(vector<Move>* ans, int color, bool enemy);
   int knightMove(vector<Move>* ans, int color, bool enemy);
   int pawnMove(vector<Move>* ans, int color, bool enemy);
   void init();
   Cstate* applyCast(Move move, int color);
   Cstate* applyPassant(Move move, int color);
   int genColumn(char c);
   int getRow(char c);
   pair<int,int> findQueen(pair<int,int>& to, int color);
   pair<int,int> findRook(pair<int,int>& to, int color, Disambiguation d);
   pair<int,int> findBishop(pair<int,int>& to, int color);
   pair<int,int> findKnight(pair<int,int>& to, int color, Disambiguation d);
   pair<int,int> findPawn(pair<int,int>& to, int color, Disambiguation d); 
   Disambiguation getDisamb(char c);
   Cstate* undo (Move& move);
   string getPosition(int x, int y);
   int getCoord(int horizontal, int vertical,int first, int second);

public:

   //Atributos
   signed char* board;
   signed char* kingData;
   unsigned char extraData;

   //Constructor
   Cstate();
   Cstate(signed char* b, signed char* kd, int ex);

   //Destructor
   virtual ~Cstate();

   //Metodos de ChessState
   virtual void generateZobristKey();
   virtual ChessState* apply(string move, int color, bool check = false);
   virtual ChessState* applyMove(Move move, int color, bool checkTerminal = false);
   virtual bool lessIllegal(int illegal_moves);
   virtual vector<moveResult> generateLegalStates(int color, bool checkTerminal = false);
   virtual vector<moveResult> generateMoves(int color);
   virtual Move genMove(string san, int color, string* p = NULL);
   virtual bool checkMate(int color, string where = "");
   virtual void print(int tab = 0);
   virtual ChessState* copy();
   virtual int numPieces(int color);
   virtual bool isMaterialWin(int color);

   //Metodos de Cstate
   int drawBoard(int tab);

   Cstate* apply(Move move, int color,bool checkTerminal = false);
   int getCastling(int color, bool derecha);
   void offCastling(int color, bool derecha);
   bool attacked(pair<int, int> posActual, int color, string* where = NULL);
   bool attack(pair<int, int> posActual, int color, string* where = NULL);

   void setPassant(int column);
   void offPassant();
   int getPassant();
   bool checkPassant();
      
};

#endif
