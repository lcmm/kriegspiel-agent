#ifndef ESTIMADOR_H
#define ESTIMADOR_H

#include "beliefState/BeliefState.h"
#include "beliefState/ChessState.h"
#include "Cstate.h"
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#define HEADER_LENGTH 19

using namespace std;

struct ResponseInfo{
   string percept;
   string checkList;
   vector<string> illegalMoves;
   int illegalSize;
};

struct Play{
   string legalMove;
   int color;
   ResponseInfo responseInfo;
};

typedef pair<Play,Play> turn;
typedef vector<turn> playset;

ResponseInfo generateInfo(string j, int color);
playset readProblemFile(string filename);
void printPlayset(playset p);
string getPos(string percept);
void estimate(playset p, BeliefState* bs);

#endif
