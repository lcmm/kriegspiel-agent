#include "Cstate.h"
#include <iostream>
#include <iterator>
#include <algorithm>

#define INITIAL_BOARD {				\
      R, N, B, Q, K, B, N, R,			\
      P, P, P, P, P, P, P, P,			\
      0, 0, 0, 0, 0, 0, 0, 0,			\
      0, 0, 0, 0, 0, 0, 0, 0,			\
      0, 0, 0, 0, 0, 0, 0, 0,			\
      0, 0, 0, 0, 0, 0, 0, 0,			\
      -P,-P,-P,-P,-P,-P,-P,-P,			\
      -R,-N,-B,-Q,-K,-B,-N,-R			\
   }

int kingPos[8][2] = { 
   {-1,-1}, 
   {-1, 0}, 
   {-1, 1}, 
   { 0, 1}, 
   { 1, 1}, 
   { 1, 0}, 
   { 1,-1}, 
   { 0,-1} 
};

string kingCheck[8] = {
   "CS",
   "CR",
   "CS",
   "CF",
   "CS",
   "CR",
   "CS",
   "CF"
};
			 
int knightPos[8][2] = { 
   {-1,-2}, 
   {-2,-1}, 
   {-2, 1}, 
   {-1, 2}, 
   { 1, 2}, 
   { 2, 1}, 
   { 2,-1}, 
   { 1,-2} 
};

bool diagoCheck [64] = {
    true,  true,  true,  true, false, false, false, false, 
    true,  true,  true,  true, false, false, false, false, 
    true,  true,  true,  true, false, false, false, false, 
    true,  true,  true,  true, false, false, false, false, 
   false, false, false, false,  true,  true,  true,  true, 
   false, false, false, false,  true,  true,  true,  true, 
   false, false, false, false,  true,  true,  true,  true, 
   false, false, false, false,  true,  true,  true,  true
};
   
void Cstate::init(){
   board = new signed char[64] INITIAL_BOARD;
   toRemove = false;
   kingData = new signed char[2] {4,60};
   extraData = 255;
   generateZobristKey();
}

Cstate::Cstate(){
   this->init();
}

Cstate::Cstate(signed char* b, signed char* kd, int ex){
   board = new signed char[64];
   for(int i=0;i<64;++i){
      board[i] = b[i];
   }
   
   kingData = new signed char[2] {kd[0],kd[1]};
   extraData = ex;
   
   generateZobristKey();
}

Cstate::~Cstate(){
   delete[] board;
   delete[] kingData;
}

//Orden del seed:
//Peones blancos * 64 casillas [-1 -> 0] [0 a 63]
//Alfiles blancos * 64 casillas [-2 -> 1] [64 a 127]
//Caballos blancos * 64 casillas [-3 -> 2] [128 a 191]
//Torres blancas * 64 casillas [-4 -> 3] [192 a 255]
//Reina blanca * 64 casillas [-5 -> 4] [256 a 319]
//Rey blanco * 64 casillas [-6 -> 5] [320 a 383]
//Peones negros * 64 casillas [1 -> 6] [384 a 447]
//Alfiles negros * 64 casillas [2 -> 7] [448 a 511]
//Caballos negros * 64 casillas [3 -> 8] [512 a 575]
//Torres negros * 64 casillas [4 -> 9] [576 a 639]
//Reina negra * 64 casillas [5 -> 10] [640 a 703]
//Rey negro * 64 casillas [6 -> 11] [704 a 767]
//De quien es el turno * 1 [NO ES USADO] [768]
//Enroques * 4 [769 a 772] [APARECE EN EXTRADATA]
//En Passants * 8 [773 a 780] [APARECE EN EXTRADATA]

void Cstate::print(int tab){
   int ans = drawBoard(tab);
   cout << endl;
}

ChessState* Cstate::copy(){
   Cstate* ans = new Cstate(this->board,this->kingData,this->extraData);
   return ans;
}

void Cstate::generateZobristKey(){
   long int zKey = 0;
   for(int i=0;i<64;++i){
      if(board[i] != 0){
	 int cur = (board[i] < 0 ? -board[i] - 1 : board[i]+5);
	 if(zKey == 0) zKey = ChessState::seed[cur * 64 + i];
	 else zKey ^= ChessState::seed[cur * 64 + i];
      }

   }
   if(this->checkPassant()){
      zKey ^= ChessState::passant_seed[this->getPassant()];
   }

   zKey ^= ChessState::castling_seed[(this->extraData & 0xF0)>>4];

   zobristKey = zKey;
}

int Cstate::genColumn(char c){
   switch(c){
   case 'a':
      return 0;
   case 'b':
      return 1;
   case 'c':
      return 2;
   case 'd':
      return 3;
   case 'e':
      return 4;
   case 'f':
      return 5;
   case 'g':
      return 6;
   case 'h':
      return 7;
   }
}

int Cstate::getRow(char c){
   switch(c){
   case '1':
      return 7;
   case '2':
      return 6;
   case '3':
      return 5;
   case '4':
      return 4;
   case '5':
      return 3;
   case '6':
      return 2;
   case '7':
      return 1;
   case '8':
      return 0;
   }
}

Disambiguation Cstate::getDisamb(char c){
   Disambiguation ans;

   switch(c){
   case 'a':
      return make_pair<bool,int>(false,0);
   case 'b':
      return make_pair<bool,int>(false,1);
   case 'c':
      return make_pair<bool,int>(false,2);
   case 'd':
      return make_pair<bool,int>(false,3);
   case 'e':
      return make_pair<bool,int>(false,4);
   case 'f':
      return make_pair<bool,int>(false,5);
   case 'g':
      return make_pair<bool,int>(false,6);
   case 'h':
      return make_pair<bool,int>(false,7);
   case '1':
      return make_pair<bool,int>(true,7);
   case '2':
      return make_pair<bool,int>(true,6);
   case '3':
      return make_pair<bool,int>(true,5);
   case '4':
      return make_pair<bool,int>(true,4);
   case '5':
      return make_pair<bool,int>(true,3);
   case '6':
      return make_pair<bool,int>(true,2);
   case '7':
      return make_pair<bool,int>(true,1);
   case '8':
      return make_pair<bool,int>(true,0);
   }
}

pair<int,int> Cstate::findQueen(pair<int,int>& to, int color){
   pair<int,int> ans;
   int q = Q*color;

   //Columnas
   for(int i=1;to.first+i<8;++i){
      int a = this->board[to.first+i + to.second*8];
      if(a == q) return make_pair(to.first+i,to.second);
      else if(a != 0 && (a>0 && q>0 || a<0 && q<0)) break;
   }
   for(int i=1;to.first-i>-1;++i){
      int a = this->board[to.first-i + to.second*8];
      if(a != 0 && a == q) return make_pair(to.first-i,to.second);
      else if(a != 0 && (a>0 && q>0 || a<0 && q<0)) break;
   }

   //Fila
   for(int i=1;to.second+i<8;++i){
      int a = this->board[to.first + (to.second+i)*8];
      if(a != 0 && a == q) return make_pair(to.first,to.second+i);
      else if(a != 0 && (a>0 && q>0 || a<0 && q<0)) break;
   }
   for(int i=1;to.second-i>-1;++i){
      int a = this->board[to.first + (to.second-i)*8];
      if(a == q) return make_pair(to.first,to.second-i);
      else if(a != 0 && (a>0 && q>0 || a<0 && q<0)) break;
   }

   //Diagonal
   //Abajo Derecha
   for(int i=1; to.first+i<8 && to.second+i<8 ; ++i){
      int a = this->board[to.first+i + (to.second+i)*8];
      if(a == q) return make_pair(to.first+i,to.second+i);
      else if(a != 0 && (a>0 && q>0 || a<0 && q<0)) break;
   }

   //Abajo Izquierda
   for(int i=1; to.first-i>-1 && to.second+i<8 ; ++i){
      int a = this->board[to.first-i + (to.second+i)*8];
      if(a == q) return make_pair(to.first-i,to.second+i);
      else if(a != 0 && (a>0 && q>0 || a<0 && q<0)) break;
   }

   //Arriba Derecha
   for(int i=1; to.first+i<8 && to.second-i>-1 ; ++i){
      int a = this->board[to.first+i + (to.second-i)*8];
      if(a == q) return make_pair(to.first+i,to.second-i);
      else if(a != 0 && (a>0 && q>0 || a<0 && q<0)) break;
   }

   //Arriba Izquierda
   for(int i=1; to.first-i>-1 && to.second-i>-1 ; ++i){
      int a = this->board[to.first-i + (to.second-i)*8];
      if(a == q) return make_pair(to.first-i,to.second-i);
      else if(a != 0 && (a>0 && q>0 || a<0 && q<0)) break;
   }   
   
   return ans;
}

pair<int,int> Cstate::findRook(pair<int,int>& to, int color, Disambiguation d){
   pair<int,int> ans;
   int r = R*color;


   if(d.second != -1){
      if(d.first){
	 for(int i=0;i<8;++i){
	    if(this->board[i + d.second*8] == r) return make_pair(i,d.second);
	 }
      }else{
	 for(int i=0;i<8;++i){
	    if(this->board[d.second + i*8] == r) return make_pair(d.second,i);
	 }
      }
   }

   //Columnas
   for(int i=1;to.first+i<8;++i){
      int a = this->board[to.first+i + to.second*8];
      if(a != 0 && a == r) return make_pair(to.first+i,to.second);
      else if(a != 0 && (a>0 && r>0 || a<0 && r<0)) break;
   }
   for(int i=1;to.first-i>-1;++i){
      int a = this->board[to.first-i + to.second*8];
      if(a != 0 && a == r) return make_pair(to.first-i,to.second);
      else if(a != 0 && (a>0 && r>0 || a<0 && r<0)) break;
   }

   //Fila
   for(int i=1;to.second+i<8;++i){
      int a = this->board[to.first + (to.second+i)*8];
      if(a != 0 && a == r) return make_pair(to.first,to.second+i);
      else if(a != 0 && (a>0 && r>0 || a<0 && r<0)) break;
   }
   for(int i=1;to.second-i>-1;++i){
      int a = this->board[to.first + (to.second-i)*8];
      if(a != 0 && a  == r) return make_pair(to.first,to.second-i);
      else if(a != 0 && (a>0 && r>0 || a<0 && r<0)) break;
   }   
   
   return ans;

}

pair<int,int> Cstate::findBishop(pair<int,int>& to, int color){
   pair<int,int> ans;
   int b = B*color;

   //Diagonal
   //Abajo Derecha
   for(int i=1; to.first+i<8 && to.second+i<8 ; ++i){
      int a = this->board[to.first+i + (to.second+i)*8];
      if(a == b) return make_pair(to.first+i,to.second+i);
      else if(a != 0 && (a>0 && b>0 || a<0 && b<0)) break;
   }

   //Abajo Izquierda
   for(int i=1; to.first-i>-1 && to.second+i<8 ; ++i){
      int a = this->board[to.first-i + (to.second+i)*8];
      if(a == b) return make_pair(to.first-i,to.second+i);
      else if(a != 0 && (a>0 && b>0 || a<0 && b<0)) break;
   }

   //Arriba Derecha
   for(int i=1; to.first+i<8 && to.second-i>-1 ; ++i){
      int a = this->board[to.first+i + (to.second-i)*8];
      if(a == b) return make_pair(to.first+i,to.second-i);
      else if(a != 0 && (a>0 && b>0 || a<0 && b<0)) break;
   }

   //Arriba Izquierda
   for(int i=1; to.first-i>-1 && to.second-i>-1 ; ++i){
      int a = this->board[to.first-i + (to.second-i)*8];
      if(a == b) return make_pair(to.first-i,to.second-i);
      else if(a != 0 && (a>0 && b>0 || a<0 && b<0)) break;
   }
 
   return ans;
}

pair<int,int> Cstate::findKnight(pair<int,int>& to, int color, Disambiguation d){
   int pos = to.first + to.second * 8;
   pair<int,int> ans;
   int n = N*color;
   
   
   for(int *i : knightPos){
      if(to.first+i[0] >= 0 && to.first+i[0] < 8 &&
	 to.second+i[1] >= 0 && to.second+i[1] < 8){
	 
	 pos = board[to.first+i[0] +(to.second+i[1])*8];
	 if(pos == n){
	    if(d.second == -1){ 
	       return make_pair(to.first+i[0],to.second+i[1]);
	    }else{
	       if(d.first){
		  if(to.second+i[1] == d.second) return make_pair(to.first+i[0],to.second+i[1]);
	       }else{
		  if(to.first+i[0] == d.second) return make_pair(to.first+i[0],to.second+i[1]);
	       }
	    }
	 }     
      }
   }

   return ans;
}   

pair<int,int> Cstate::findPawn(pair<int,int>& to, int color, Disambiguation d){
   int pos = to.first + to.second * 8;
   pair<int,int> ans;

   if(d.second != -1) return make_pair(d.second,to.second +(color == WHITE ? 1 : -1));
   if(color == WHITE){
      if(pos+8 < 64 && board[pos+8] == -P) return make_pair(to.first,to.second+1);
      return make_pair(to.first,to.second+2);
   }
   if(pos-8 > 0 && board[pos-8] == P) return make_pair(to.first,to.second-1);
   return make_pair(to.first,to.second-2);
}

Move Cstate::genMove(string san, int color, string* p){
   Move ans;
   int pieceToMove;
   int cur = 0;
   ans.capture = false;
   for(char a : san){
      if(a == 'x'){
	 ans.capture = true;
	 break;
      }
   }
   pair<bool,int> disamb{false,-1};

   switch(san[cur]){
   case 'K':
      pieceToMove = K;
      if(san[++cur] == 'x'){
	 ++cur;
      }
      ans.to.first = genColumn(san[cur++]);
      ans.to.second = getRow(san[cur]);
      ans.from.first = kingData[(color == -1 ? 1 : 0)] % 8;
      ans.from.second = kingData[(color == -1 ? 1 : 0)] / 8;
      break;
   case 'Q':
      pieceToMove = Q;
      if(san[++cur] == 'x'){
	 ++cur;
      }
      ans.to.first = genColumn(san[cur++]);
      ans.to.second = getRow(san[cur]);
      ans.from = findQueen(ans.to,color);
      break;
   case 'R':
      pieceToMove = R;
      if(san.size() == 5 && san[san.size()-1] != '+' || san.size() == 6){
	 disamb = getDisamb(san[++cur]);
	 ++cur;
      }else if(san.size() == 4 && san[san.size()-1] != '+' || san.size() == 5){
	 if(san[++cur] == 'x'){}
	 else disamb = getDisamb(san[cur]);
      }

      ans.to.first = genColumn(san[++cur]);
      ans.to.second = getRow(san[++cur]);
      ans.from = findRook(ans.to, color,disamb);
      break;
   case 'B':
      pieceToMove = B;
      if(san[++cur] == 'x') ++cur;
      ans.to.first = genColumn(san[cur++]);
      ans.to.second = getRow(san[cur]);
      ans.from = findBishop(ans.to,color);
      break;
   case 'N':
      pieceToMove = N;
      if(san.size() == 5 && san[san.size()-1] != '+' || san.size() == 6){
	 disamb = getDisamb(san[++cur]);
	 ++cur;
      }else if(san.size() == 4 && san[san.size()-1] != '+' || san.size() == 5){
	 if(san[++cur] == 'x'){}
	 else disamb = getDisamb(san[cur]);
      }

      ans.to.first = genColumn(san[++cur]);
      ans.to.second = getRow(san[++cur]);
      ans.from = findKnight(ans.to, color, disamb);
      break;
   default:
      pieceToMove = P;
      if(san.size() == 4 && san[san.size()-1] != '+' || san.size() == 5){
	 disamb = getDisamb(san[cur++]);
	 ++cur;
      }else if(san.size() == 3 && san[san.size()-1] != '+' || san.size() == 4){
	 disamb = getDisamb(san[cur++]);
      }

      ans.to.first = genColumn(san[cur++]);
      ans.to.second = getRow(san[cur]);
      ans.from = findPawn(ans.to,color,disamb);
      break;
   }

   if( p != NULL){
      if(pieceToMove == P && this->checkPassant()){
	 int posPassant = this->getPassant();
	 if(ans.from.second == (color == WHITE ? 3 : 4) &&
	    (ans.from.first-1 == posPassant || ans.from.first+1 == posPassant) &&
	    ans.to.first == posPassant
	    ){
	    *p = this->getPosition(ans.to.first,ans.from.second);
	 }else{
	    *p = this->getPosition(ans.to.first,ans.to.second);
	 }
      }else{
	 *p = this->getPosition(ans.to.first,ans.to.second);
      }
   }
   
   return ans;
}

ChessState* Cstate::apply(string m, int color, bool check){
   Move move = genMove(m,color);
   move.en_passant = -1;move.castle=false;move.promotion=false;move.passant=false;

   if(check && this->board[move.to.second*8 + move.to.first] != 0)      
      move.capture = true;
   
   return (ChessState*) (this->applyMove(move,color,check));
}

ChessState* Cstate::applyMove(Move move, int color, bool checkTerminal){
   Cstate* ans = new Cstate(this->board,this->kingData,this->extraData);
   int from = move.from.second*8 + move.from.first;
   int to = move.to.second*8 + move.to.first;
   int passant = (color == WHITE ? 3 : 4);
   int start = (color == WHITE ? 6 : 1);
   int forward = (color == WHITE ? -1 : 1);

   signed char pieceToMove  = ans->board[from];
   
   if(ans->board[to] < 0 && color == WHITE || ans->board[to] > 0 && color == BLACK){
      delete(ans);
      return nullptr;
   }

   if(pieceToMove == P*color && this->checkPassant()){
      int posPassant = this->getPassant();
      
      if(move.from.second == passant &&
	 (move.from.first-1 == posPassant || move.from.first+1 == posPassant) &&
	 move.to.first == posPassant
	 ){
	 
	 return this->applyPassant(move,color);
      }
   }
   
   if(move.capture && (ans->board[to] == 0)){
      return nullptr;
   }
   if(!move.capture && (ans->board[to] != 0)){
      return nullptr;
   }
   
   // pendiente para en_passant
   if(pieceToMove == P*color && move.from.first != move.to.first &&
      move.from.second != move.to.second && ans->board[to] == 0)
      return nullptr;
      
   if(pieceToMove == P*color && move.from.first == move.to.first && 
      move.from.second != move.to.second && ans->board[to] != 0)
      return nullptr;

   if(pieceToMove != N*color){
      if(move.from.second == move.to.second){
	 if(from < to)
	    for(int i=from+1;i<to;++i) {if(ans->board[i] != 0) return nullptr;}
	 else
	    for(int i=from-1;i>to;--i) {if(ans->board[i] != 0) return nullptr;}
      }else if(move.from.first == move.to.first){
	 if(from < to) 
	    for(int i=from+8;i<to;i+=8) {if(ans->board[i] != 0) return nullptr;}
	 else{
	    for(int i=from-8;i>to;i-=8) {if(ans->board[i] != 0) return nullptr;}
	 }
      }else if(move.from.first < move.to.first){
	 if(move.from.second > move.to.second)
	    for(int i=from-7;i>to;i-=7) {if(ans->board[i] != 0) return nullptr;}
	 else
	    for(int i=from+9;i<to;i+=9) {if(ans->board[i] != 0) return nullptr;}
      }else{
	 if(move.from.second > move.to.second)
	    for(int i=from-9;i>to;i-=9) {if(ans->board[i] != 0) return nullptr;}
	 else
	    for(int i=from+7;i<to;i+=7) {if(ans->board[i] != 0) return nullptr;}
      }
   }

   if(pieceToMove == P*color && move.from.second == start && move.to.second == move.from.second+2*forward){
      move.en_passant = move.to.first;
   } else {
      move.en_passant = -1;
   }

   return (ChessState*) (this->apply(move,color,checkTerminal));

}

Cstate* Cstate::apply(Move move, int color, bool checkTerminal){

   Cstate* ans = new Cstate(this->board,this->kingData,this->extraData);
   int from = move.from.second*8 + move.from.first;
   int to = move.to.second*8 + move.to.first;
   
   signed char pieceToMove  = ans->board[from];

   if(ans->board[to] == -R || ans->board[to] == R){
      if(move.to.first == 0){	 
	 ans->offCastling(-color,false);
      }else if(move.to.first == 7){
	 ans->offCastling(-color,true);
      }
   }
  
   ans->board[to] = pieceToMove;
   ans->board[from] = 0;

   if(pieceToMove == K){
      ans->kingData[0] = to;
      ans->offCastling(color,true);
      ans->offCastling(color,false);
   }else if(pieceToMove == -K){
      ans->kingData[1] = to;
      ans->offCastling(color,true);
      ans->offCastling(color,false);
   }else if(pieceToMove == R  || pieceToMove == -R){
      if(move.from.first == 0){
	 ans->offCastling(color,false);
      }else if(move.from.first == 7){
	 ans->offCastling(color,true);
      }
   }
   
   if(ans->checkMate(color)){
      delete(ans);
      return nullptr;
   }

   ans->setPassant(move.en_passant);
   ans->generateZobristKey();
   ans->legal = true;

   if(checkTerminal && ans->generateLegalStates(-color).size() == 0){
      ans->terminal = true;
      if(ans->checkMate(-color)){
	 ans->winner = color;
      }
   }

   return ans;
}

Cstate* Cstate::applyCast(Move move, int color){
   Cstate* ans = this->apply(move, color);
   int to = move.to.second*8 + move.to.first;
   
   ans->board[(move.to.first == 5 ? to+1 : to - 1)] = color*K;
   ans->board[(color == WHITE ? ans->kingData[1] : ans->kingData[0])] = 0;
   ans->kingData[(color == WHITE ? 1 : 0)] = (move.to.first == 5 ? to+1 : to-1);

   return ans;
}

Cstate* Cstate::applyPassant(Move move, int color){
   Cstate* ans = this->apply(move, color);
   if(ans == nullptr)
      return ans;
   
   int pawn = move.from.second*8 + move.to.first;
      
   if(ans->board[pawn] == (-color)*P){
      ans->board[pawn] = 0;

      if(ans->checkMate(color)){
	 delete(ans);
	 return nullptr;
      }

   }else{
      delete(ans);
      return nullptr;
   }
   
         
   return ans;
}



bool Cstate::lessIllegal(int illegal_moves){
   return false;
}

void Cstate::setPassant(int column){
   if(column == -1){
      this->offPassant();
   }else{
      column |= 0xF0;
      this->extraData |= 0x0F;
      this->extraData &= column;
   }
}

void Cstate::offPassant()
{
   this->extraData &= 0xF0;
   this->extraData |= 0x0F;
   
}

int Cstate::getPassant(){
   return (this -> extraData & 0x0F);
}

bool Cstate::checkPassant(){
   return !(this -> extraData & 0x08);
}



void Cstate::offCastling(int color, bool derecha){
   if(color == WHITE)
      if(derecha)//derecho
	 this->extraData &= 0x7F;
      else //izquierdo
	 this->extraData &= 0xBF;
   else
      if(derecha)//derecho
	 this->extraData &= 0xDF;
      else //izquierdo
	 this->extraData &= 0xEF;  
}


int Cstate::getCastling(int color, bool derecha){

   int n;
   if(color == WHITE)
      if(derecha)//derecho
	 n = this->extraData >> 7;
      else //izquierdo
	 n = (this->extraData >> 6)&1;
   else
      if(derecha)//derecho
	 n = (this->extraData >> 5)&1;
      else //izquierdo
	 n = (this->extraData >> 4)&1;

   return n;
}

int Cstate::getPieceColor(int piece){
   if(piece > 0) return BLACK;
   else if(piece < 0) return WHITE;
   else return 0;
}

vector<moveResult> Cstate::generateLegalStates(int color, bool checkTerminal){

   vector<moveResult> ans;
   vector<Move> a;
   this->illegal = 0;

   this->illegal += this->kingMove(&a,color, false);
   this->illegal += this->rookMove(&a,color, false);
   this->illegal += this->bishopMove(&a,color, false);
   this->illegal += this->queenMove(&a,color, false);
   this->illegal += this->knightMove(&a,color, false);
   this->illegal += this->pawnMove(&a,color, false);

   moveResult dummy;
   for(Move i : a){
      if(i.castle){
	 dummy.state = this->applyCast(i,color);
	 dummy.capture = false;
	 dummy.to = "castling";
	 dummy.state->generateZobristKey();
	 ans.push_back(dummy);
      }
      else if(i.promotion){
	 for(int piece = B; piece < K; piece++){
	    dummy.state = this->apply(i,color);
	    if(dummy.state != nullptr){
	       ( (Cstate*) (dummy.state) )->board[i.to.second*8+i.to.first] = color*piece;
	       dummy.capture = this->board[i.to.second*8+i.to.first] != 0;
	       dummy.to = getPosition(i.to.first,i.to.second);
	       dummy.state->generateZobristKey();
	       ans.push_back(dummy);
	    }else this->illegal += 1;
	 }
      }else if(i.passant){
      	 dummy.state = this->applyPassant(i,color);
	 if(dummy.state != nullptr){
	    dummy.capture = true;
	    dummy.to = getPosition(i.to.first,i.from.second);
	    dummy.state->generateZobristKey();
	    ans.push_back(dummy);
	 }else this->illegal += 1;	 
      }else{
	 dummy.state = this->apply(i,color);
	 if(dummy.state != nullptr){
	    dummy.capture = this->board[i.to.second*8+i.to.first] != 0;
	    dummy.to = getPosition(i.to.first,i.to.second);
	    ans.push_back(dummy);
	 }else this->illegal += 1;
      }
   }
   
   return ans;
}

vector<moveResult> Cstate::generateMoves(int color){
   vector<moveResult> ans;
   vector<Move> a;
   this->kingMove(&a,color, true);
   this->rookMove(&a,color, true);
   this->bishopMove(&a,color, true);
   this->queenMove(&a,color, true);
   this->knightMove(&a,color, true);
   this->pawnMove(&a,color, true);

   moveResult dummy;
   for(Move i : a){
      dummy.move = i;
      if(i.castle){
	 dummy.capture = false;
	 dummy.to = "castling";
	 ans.push_back(dummy);
      }else if(i.passant){
	 dummy.capture = true;
	 dummy.to = getPosition(i.to.first,i.from.second);
	 ans.push_back(dummy);
      }else{
	 dummy.capture = this->board[i.to.second*8+i.to.first] != 0;
	 dummy.to = getPosition(i.to.first,i.to.second);
	 ans.push_back(dummy);
      }
   }
   return ans;

}

string Cstate::getPosition(int x, int y){
   string res = "";
   switch(x){
   case 0:
      res.push_back('a');
      break;
   case 1:
      res.push_back('b');
      break;
   case 2:
      res.push_back('c');
      break;
   case 3:
      res.push_back('d');
      break;
   case 4:
      res.push_back('e');
      break;
   case 5:
      res.push_back('f');
      break;
   case 6:
      res.push_back('g');
      break;
   case 7:
      res.push_back('h');
      break;
   }
   switch(y){
   case 0:
      res.push_back('8');
      break;
   case 1:
      res.push_back('7');
      break;
   case 2:
      res.push_back('6');
      break;
   case 3:
      res.push_back('5');
      break;
   case 4:
      res.push_back('4');
      break;
   case 5:
      res.push_back('3');
      break;
   case 6:
      res.push_back('2');
      break;
   case 7:
      res.push_back('1');
      break;
   }
   return res;
}

int Cstate::getPiece(int p, int color){
   int pos = -1;
   for(int i=0;i<64;i++)
      if(this->board[i]==color*p){
	 pos = i;
	 break;
      }
   return pos;
}

int Cstate::kingMove(vector<Move>* ans, int color, bool enemy){
   int illegal = 0;

   int pos = color == WHITE ? kingData[1] : kingData[0];
   pair<int,int> posActual = make_pair(pos%8,pos/8);
         
   for(int* i : kingPos){
      Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;

      if(posActual.first+i[0]>=0 && posActual.first+i[0]<8 &&
	 posActual.second+i[1]>=0 && posActual.second+i[1]<8){
	 int a = getPieceColor(this->board[(posActual.second+i[1])*8+posActual.first+i[0]]);	    
	 if( enemy || a != color ){
	    m.from = posActual;
	    m.to = make_pair(posActual.first+i[0],posActual.second+i[1]);
	    ans->push_back(m);
	 }else{
	    ++illegal;
	 }
      }
   }
   
   bool cast = true;
   if(this->getCastling(color, true)){
      
      // Que no haya nadie las casillas
      for(int i=1; posActual.first+i<7 && cast; i++){
	 int a = this->board[(posActual.second)*8+posActual.first+i];
	 if(a!=0){
	    cast = false;
	 }
      }
      // Que las casillas no esten siendo atacadas
      if(!enemy){
	 for(int i=0; posActual.first+i<7 && cast; i++){
	    if(this->attack(make_pair(posActual.first+i,posActual.second),color)){
	       cast = false;
	    }
	 }
      }
      if(cast){
	 Move m; m.en_passant = -1;m.castle=true;
	 m.from = make_pair(7,posActual.second);
	 m.to = make_pair(5,posActual.second);
	 ans->push_back(m);
      }else{
	 ++illegal;
      }
   }
   cast = true;
   if(this->getCastling(color, false)){
      // Que no haya nadie las casillas
      for(int i=1; posActual.first-i>=1 && cast; i++){
	 int a = this->board[(posActual.second)*8+posActual.first-i];
	 if(a!=0){
	    cast = false;
	 }
      }
      // Que las casillas no esten siendo atacadas
      if(!enemy){
	 for(int i=0; posActual.first-i>=2 && cast; i++){
	    if(this->attack(make_pair(posActual.first-i,posActual.second),color)){
	       cast = false;
	    }
	 }
      }

      if(cast){
	 Move m; m.en_passant = -1;m.castle=true;
	 m.from = make_pair(0,posActual.second);
	 m.to = make_pair(3,posActual.second);
	 ans->push_back(m);
      }else{
	 ++illegal;
      }

   }
   
   return illegal;
}

int Cstate::rookMove(vector<Move>* ans, int color, bool enemy){
   int illegal = 0;

   for(int pos=0;pos<64;pos++)
      if(this->board[pos]==color*R){
	 pair<int,int> posActual = make_pair(pos%8,pos/8);
	  
	 //izquierda
	 for(int i = posActual.first-1; i >= 0 ; i-- ){
	    Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
	    int a = getPieceColor(this->board[(posActual.second)*8+i]);
	    if(!enemy && a == color){
//	       illegal += i;
	       break;
	    }else{
	       m.from = posActual;
	       m.to = make_pair(i,posActual.second);
	       ans->push_back(m);
	       if(!enemy && a != 0 ){
		  illegal += i;
		  break;
	       }
	    }
	 }

	 //derecha
	 for(int i = posActual.first+1; i < 8 ; i++ ){
	    Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
	    int a = getPieceColor(this->board[(posActual.second)*8+i]);
	    if(!enemy && a == color){
//	       illegal += 7-i;
	       break;
	    }else{
	       m.from = posActual;
	       m.to = make_pair(i,posActual.second);
	       ans->push_back(m);
	       if(!enemy && a != 0){
		  illegal += 7-i;
		  break;
	       }
	    } 
	 }

	 //arriba
	 for(int i = posActual.second-1; i >= 0 ; i-- ){
	    Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
	    int a = getPieceColor(this->board[(i)*8+posActual.first]);
	    if(!enemy && a == color){
//	       illegal += i;
	       break;
	    }else{
	       m.from = posActual;
	       m.to = make_pair(posActual.first,i);
	       ans->push_back(m);
	       if(!enemy && a != 0 ){
		  illegal += i;
		  break;
	       }
	    }
	 }
	 //abajo
	 for(int i = posActual.second+1; i < 8 ; i++ ){
	    Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
	    int a = this->board[(i)*8+posActual.first];
	    if(!enemy && a == color){
//	       illegal += 7-i;
	       break;
	    }else{
	       m.from = posActual;
	       m.to = make_pair(posActual.first,i);
	       ans->push_back(m);
	       if(!enemy && a != 0 ){
		  illegal += 7-i;
		  break;
	       }
	    } 
	 }
      }

   return illegal;
}

int Cstate::getCoord(int horizontal, int vertical, int first, int second){
   if(horizontal == RIGHT){
      if(vertical == DOWN){
         if(first > second) return first;
         else return second;
      }else if(vertical == UP){
         if(first+second >= 7) return first;
         else return second;
      }
   }else if(horizontal == LEFT){
      if(vertical == DOWN){
         if(first+second >= 7) return second;
         else return first;
      }else if(vertical == UP){
         if(first > second) return second;
         else return first;
      }
   }
}

int Cstate::bishopMove(vector<Move>* ans, int color, bool enemy){
   int illegal = 0;

   for(int pos=0;pos<64;pos++)
      if(this->board[pos]==color*B){

	 pair<int,int> posActual = make_pair(pos%8,pos/8);

	 // abajo - derecha
         for(int i=1; posActual.first+i<8 && posActual.second+i<8 ; i++){
            Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
            int a = getPieceColor(this->board[(posActual.second+i)*8+posActual.first+i]);
            if( !enemy && a == color){
               if(getCoord(RIGHT,DOWN,posActual.first+i,posActual.second+i) == posActual.first+i)
                  illegal += 7-posActual.first+i;
               else illegal += 7-posActual.second+i;
               break;
            }else{
               m.from = posActual;
               m.to = make_pair(posActual.first+i,posActual.second+i);
               ans->push_back(m);
               if( !enemy && a != 0 ){
                  if(getCoord(RIGHT,DOWN,posActual.first+i,posActual.second+i) == posActual.first+i)
                     illegal += 7-posActual.first+i;
                  else illegal += 7-posActual.second+i;
                  break;
               }
            }
         }	
 
	 // abajo - izq
         for(int i=1; posActual.first-i>=0 && posActual.second+i<8 ; i++){
            Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
            int a = getPieceColor(this->board[(posActual.second+i)*8+posActual.first-i]);
            if(!enemy && a == color){
               if(getCoord(LEFT,DOWN,posActual.first-i,posActual.second+i) == posActual.first-i)
                  illegal += posActual.first-i;
               else illegal += 7-posActual.second+i;
               break;
            }else{
               m.from = posActual;
               m.to = make_pair(posActual.first-i,posActual.second+i);
               ans->push_back(m);
               if(!enemy && a != 0 ){
                  if(getCoord(LEFT,DOWN,posActual.first-i,posActual.second+i) == posActual.first-i)
                     illegal += posActual.first-i;
                  else illegal += 7-posActual.second+i;
                  break;
               }
            }
         }

	 // arriba - derecha
         for(int i=1; posActual.first+i<8 && posActual.second-i>=0 ; i++){
            Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
            int a = getPieceColor(this->board[(posActual.second-i)*8+posActual.first+i]);
            if(!enemy &&  a == color){
               if(getCoord(RIGHT,UP,posActual.first+i,posActual.second-i) == posActual.first+i)
                  illegal += 7-posActual.first+i;
               else illegal += posActual.second-i;
               break;
            }else{
               m.from = posActual;
               m.to = make_pair(posActual.first+i,posActual.second-i);
               ans->push_back(m);
               if(!enemy && a != 0 ){
                  if(getCoord(RIGHT,UP,posActual.first+i,posActual.second-i) == posActual.first+i)
                     illegal += 7-posActual.first+i;
                  else illegal += posActual.second-i;
                  break;
               }
            }
         } 

	 // arriba - izquierda
         for(int i=1; posActual.first-i>=0 && posActual.second-i>=0 ; i++){
            Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
            int a = getPieceColor(this->board[(posActual.second-i)*8+posActual.first-i]);
            if(!enemy && a == color){
               if(getCoord(LEFT,UP,posActual.first-i,posActual.second-i) == posActual.first-i)
                  illegal += posActual.first-i;
               else illegal += posActual.second-i;
               break;
            }else{
               m.from = posActual;
               m.to = make_pair(posActual.first-i,posActual.second-i);
               ans->push_back(m);
               if(!enemy && a != 0 ){
                  if(getCoord(LEFT,UP,posActual.first-i,posActual.second-i) == posActual.first-i)
                     illegal += posActual.first-i;
                  else illegal += posActual.second-i;
                  break;
               }
            }
         }	
 
      }

   return illegal;
}

int Cstate::queenMove(vector<Move>* ans, int color, bool enemy){
   int illegal = 0;

   for(int pos=0;pos<64;pos++)
      if(this->board[pos]==color*Q){

	 pair<int,int> posActual = make_pair(pos%8,pos/8);

	 //izquierda
	 for(int i = posActual.first-1; i >= 0 ; i-- ){
	    Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
	    int a = getPieceColor(this->board[(posActual.second)*8+i]);
	    if(!enemy && a == color){
	       illegal += i;
	       break;
	    }else{
	       m.from = posActual;
	       m.to = make_pair(i,posActual.second);
	       ans->push_back(m);
	       if(!enemy && a != 0 ){
		  illegal += i;
		  break;
	       }
	    }
	 }

	 //derecha
	 for(int i = posActual.first+1; i < 8 ; i++ ){
	    Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
	    int a = getPieceColor(this->board[(posActual.second)*8+i]);
	    if(!enemy && a == color){
	       illegal += 7-i;
	       break;
	    }else{
	       m.from = posActual;
	       m.to = make_pair(i,posActual.second);
	       ans->push_back(m);
	       if(!enemy &&  a != 0 ){
		  illegal += 7-i;
		  break;
	       }
	    } 
	 }

	 //arriba
	 for(int i = posActual.second-1; i >= 0 ; i-- ){
	    Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
	    int a = getPieceColor(this->board[(i)*8+posActual.first]);
	    if(!enemy && a == color){
	       illegal += i;
	       break;
	    }else{
	       m.from = posActual;
	       m.to = make_pair(posActual.first,i);
	       ans->push_back(m);
	       if(!enemy && a != 0 ){
		  illegal += i;
		  break;
	       }
	    }
	 }

	 //abajo
	 for(int i = posActual.second+1; i < 8 ; i++ ){
	    Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
	    int a = this->board[(i)*8+posActual.first];
	    if(!enemy && a == color){
	       illegal += 7-i;
	       break;
	    }else{
	       m.from = posActual;
	       m.to = make_pair(posActual.first,i);
	       ans->push_back(m);
	       if(!enemy && a != 0 ){
		  illegal += 7-i;
		  break;
	       }
	    } 
	 }
   
	 // abajo - derecha
         for(int i=1; posActual.first+i<8 && posActual.second+i<8 ; i++){
            Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
            int a = getPieceColor(this->board[(posActual.second+i)*8+posActual.first+i]);
            if(!enemy && a == color){
               if(getCoord(RIGHT,DOWN,posActual.first+i,posActual.second+i) == posActual.first+i)
                  illegal += 7-posActual.first+i;
               else illegal += 7-posActual.second+i;
               break;
            }else{
               m.from = posActual;
               m.to = make_pair(posActual.first+i,posActual.second+i);
               ans->push_back(m);
               if(!enemy && a != 0 ){
                  if(getCoord(RIGHT,DOWN,posActual.first+i,posActual.second+i) == posActual.first+i)
                     illegal += 7-posActual.first+i;
                  else illegal += 7-posActual.second+i;
                  break;
               }
            }
         }	
 
	 // abajo - izq
         for(int i=1; posActual.first-i>=0 && posActual.second+i<8 ; i++){
            Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
            int a = getPieceColor(this->board[(posActual.second+i)*8+posActual.first-i]);
            if(!enemy && a == color){
               if(getCoord(LEFT,DOWN,posActual.first-i,posActual.second+i) == posActual.first-i)
                  illegal += posActual.first-i;
               else illegal += 7-posActual.second+i;
               break;
            }else{
               m.from = posActual;
               m.to = make_pair(posActual.first-i,posActual.second+i);
               ans->push_back(m);
               if(!enemy && a != 0 ){
                  if(getCoord(LEFT,DOWN,posActual.first-i,posActual.second+i) == posActual.first-i)
                     illegal += posActual.first-i;
                  else illegal += 7-posActual.second+i;
                  break;
               }
            }
         }

	 // arriba - derecha
         for(int i=1; posActual.first+i<8 && posActual.second-i>=0 ; i++){
            Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
            int a = getPieceColor(this->board[(posActual.second-i)*8+posActual.first+i]);
            if(!enemy && a == color){
               if(getCoord(RIGHT,UP,posActual.first+i,posActual.second-i) == posActual.first+i)
                  illegal += 7-posActual.first+i;
               else illegal += posActual.second-i;
               break;
            }else{
               m.from = posActual;
               m.to = make_pair(posActual.first+i,posActual.second-i);
               ans->push_back(m);
               if(!enemy && a != 0 ){
                  if(getCoord(RIGHT,UP,posActual.first+i,posActual.second-i) == posActual.first+i)
                     illegal += 7-posActual.first+i;
                  else illegal += posActual.second-i;
                  break;
               }
            }
         } 

	 // arriba - izquierda
         for(int i=1; posActual.first-i>=0 && posActual.second-i>=0 ; i++){
            Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
            int a = getPieceColor(this->board[(posActual.second-i)*8+posActual.first-i]);
            if(!enemy && a == color){
               if(getCoord(LEFT,UP,posActual.first-i,posActual.second-i) == posActual.first-i)
                  illegal += posActual.first-i;
               else illegal += posActual.second-i;
               break;
            }else{
               m.from = posActual;
               m.to = make_pair(posActual.first-i,posActual.second-i);
               ans->push_back(m);
               if(!enemy && a != 0 ){
                  if(getCoord(LEFT,UP,posActual.first-i,posActual.second-i) == posActual.first-i)
                     illegal += posActual.first-i;
                  else illegal += posActual.second-i;
                  break;
               }
            }
         }
      }

   return illegal;
}


int Cstate::knightMove(vector<Move>* ans, int color, bool enemy){
   int illegal = 0;
   for(int pos=0;pos<64;pos++)
      if(this->board[pos]==color*N){

	 pair<int,int> posActual = make_pair(pos%8,pos/8);

	 for(int* i : knightPos){
	    Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;

	    if(posActual.first+i[0]>=0 && posActual.first+i[0]<8 &&
	       posActual.second+i[1]>=0 && posActual.second+i[1]<8){
	       int a = getPieceColor(this->board[(posActual.second+i[1])*8+posActual.first+i[0]]);
	       if(enemy || a != color ){
		  m.from = posActual;
		  m.to = make_pair(posActual.first+i[0],posActual.second+i[1]);
		  ans->push_back(m);
	       }else ++illegal;
	    }
	 }
      }

   return illegal;
}

int Cstate::pawnMove(vector<Move>* ans, int color, bool enemy){
   int forward = (color == WHITE ? -1 : 1);
   int goal = (color == WHITE ? 0 : 7);
   int start = (color == WHITE ? 6 : 1);
   int passant = (color == WHITE ? 3 : 4);

   int illegal = 0;
   int illegalEat = 0;
   bool comi = false;
      
   for(int pos=0;pos<64;pos++)
      if(this->board[pos]==color*P){
	 pair<int,int> posActual = make_pair(pos%8,pos/8);
	 Move m; m.en_passant = -1;m.castle=false;m.promotion=false;m.capture=false;m.passant=false;
	 	 	 
	 if(posActual.second+forward != color){
	    int a = getPieceColor(this->board[(posActual.second+forward)*8+posActual.first]);
	    if(enemy || a == 0){
	       //promotion
	       m.promotion = posActual.second+forward == goal;

	       m.from = posActual;
	       m.to = posActual;
	       m.to.second = posActual.second+forward;
	       ans->push_back(m);
	       if(posActual.second == start){
		  a = getPieceColor(this->board[(posActual.second+2*forward)*8+posActual.first]);
		  if(enemy || a == 0){
		     m.from = posActual;
		     m.to = posActual;
		     m.to.second = posActual.second+2*forward;
		     m.en_passant = posActual.first;
		     ans->push_back(m);
		  }else if(a != color) ++illegal;
	       }
	    }else if(a != color) ++illegal;
	    for(int i : {-1, 1})
	       if(posActual.first+i>=0 && posActual.first+i<8){
		  int a = getPieceColor(this->board[(posActual.second+forward)*8+posActual.first+i]);
		  if(a == -color){
		     m.from = posActual;
		     m.to = make_pair(posActual.first+i,posActual.second+forward);

		     //promotion
		     m.promotion = posActual.second+forward == goal;

		     ans->push_back(m);

		     comi = true;
		  }else if(!enemy) ++illegalEat;
	       }
	    if(this->checkPassant()){
	       int posPassant = this->getPassant();
	       
	       if(posActual.second == passant &&
		  (posActual.first-1 == posPassant || posActual.first+1 == posPassant)){
		  m.passant = true;
		  m.from = posActual;
		  m.to = make_pair(posPassant, passant+forward);
		  ans->push_back(m);
	       }
	    }
	 } 	 
      }
      illegal += illegalEat;
   
   return illegal;
}

bool Cstate::checkMate(int color, string where){
   int pos = (color == WHITE ? kingData[1] : kingData[0]);
   pair<int,int> posActual = make_pair(pos%8,pos/8);

  if (where == "")
      return this->attacked(posActual, color);
  else{
      string ans;
      bool chk = this->attacked(posActual, color, &ans);
      return chk && (ans == where);
   }
}

bool Cstate::attacked(pair<int, int> posActual, int color, string* where){
   bool ans = false;
   string acum = "C";
   
   //arriba
   for(int i = posActual.second-1; i >= 0 ; i-- ){
      int a = this->board[(i)*8+posActual.first];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if((i == (posActual.second-1) && a == K) || a == Q || a == R ){
	       if(where != NULL){
		  acum.push_back('F');
	       }
	       ans = true;
	    }
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if((i == (posActual.second-1) && -a == K) || -a == Q || -a == R ){
	       if(where != NULL){
		  acum.push_back('F');
	       }
	       ans = true;
	    }
	    break;
	 }
      }
   }

   //abajo
   for(int i = posActual.second+1; i < 8 ; i++ ){
      int a = this->board[(i)*8+posActual.first];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if((i == (posActual.second+1) && a == K) || a == Q || a == R ){
	       if(where != NULL){
		  acum.push_back('F');
	       }
	       ans = true;
	    }
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if((i == (posActual.second+1) && -a == K) || -a == Q || -a == R ){
	       if(where != NULL){
		  acum.push_back('F');
	       }
	       ans = true;
	    }
	    break;
	 }
      }
   }
      
   //caballo
   for(int* i : knightPos){
      if(posActual.first+i[0]>=0 && posActual.first+i[0]<8 &&
	 posActual.second+i[1]>=0 && posActual.second+i[1]<8){
	 int a = this->board[(posActual.second+i[1])*8+posActual.first+i[0]];
	 if(color<0){
	    if( a > 0 ){
	       if( a == N ){
		  if(where != NULL){
		     acum.push_back('N');
		  }
		  ans = true;
	       }
	    }	 
	 }else{
	    if( a < 0 ){
	       if(-a == N ){
		  if(where != NULL){
		     acum.push_back('N');
		  }
		  ans = true;
	       }
	    }
	 }
      }
   }
   
   //izquierda
   for(int i = posActual.first-1; i >= 0 ; i-- ){
      int a = this->board[(posActual.second)*8+i];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if((i == (posActual.first-1) && a == K) || a == Q || a == R ){
	       if(where != NULL){
		  acum.push_back('R');
	       }
	       ans = true;
	    }
	    
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if((i == (posActual.first-1) && -a == K) || -a == Q || -a == R ){
	       if(where != NULL){
		  acum.push_back('R');
	       }
	       ans = true;
	    }
	    break;
	 }
      }
   }

   //derecha
   for(int i = posActual.first+1; i < 8 ; i++ ){
      int a = this->board[(posActual.second)*8+i];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if((i == (posActual.first+1) && a == K) || a == Q || a == R ){
	       if(where != NULL){
		  acum.push_back('R');
	       }
	       ans = true;
	    }
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if((i == (posActual.first+1) && -a == K) || -a == Q || -a == R ){
	       if(where != NULL){
		  acum.push_back('R');
	       }
	       ans = true;
	    }
	    break;
	 }
      }
   }


   //revisa que diagonal es short o long
   if(diagoCheck[(posActual.second)*8+posActual.first]){
      //revisa primero arriba-izq y abajo derecha
      // arriba - izquierda
      for(int i=1; posActual.first-i>=0 && posActual.second-i>=0 ; i++){
	 int a = this->board[(posActual.second-i)*8+posActual.first-i];
	 if(color<0){
	    if( a < 0){
	       break;
	    }else if( a > 0 ){
	       if((i == 1 && (a == K || a == P)) || a == Q || a == B){
		  if(where != NULL){
		     acum.push_back('L');
		  }
		  ans = true;
	       }
	       break;
	    }	 
	 }else{
	    if( a > 0){
	       break;
	    }else if( a < 0 ){
	       if((i == 1 && -a == K) || -a == Q || -a == B ){
		  if(where != NULL){
		     acum.push_back('L');
		  }
		  ans = true;
	       }
	       break;
	    }
	 }
      }

      // abajo - derecha
      for(int i=1; posActual.first+i<8 && posActual.second+i<8 ; i++){
	 int a = this->board[(posActual.second+i)*8+posActual.first+i];
	 if(color<0){
	    if( a < 0){
	       break;
	    }else if( a > 0 ){
	       if((i == 1 && a == K) || a == Q || a == B ){
		  if(where != NULL){
		     acum.push_back('L');
		  }
		  ans = true;
	       }
	       break;
	    }	 
	 }else{
	    if( a > 0){
	       break;
	    }else if( a < 0 ){
	       if((i == 1 && (-a == K || -a == P)) || -a == Q || -a == B ){
		  if(where != NULL){
		     acum.push_back('L');
		  }
		  ans = true;
	       }
	       break;
	    }
	 }
      }
      // abajo - izq
      for(int i=1; posActual.first-i>=0 && posActual.second+i<8 ; i++){
	 int a = this->board[(posActual.second+i)*8+posActual.first-i]; 
	 if(color<0){
	    if( a < 0){
	       break;
	    }else if( a > 0 ){
	       if((i == 1 && a == K) || a == Q || a == B ){
		  if(where != NULL){
		     acum.push_back('S');
		  }
		  ans = true;
	       }
	       break;
	    }	 
	 }else{
	    if( a > 0){
	       break;
	    }else if( a < 0 ){
	       if((i == 1 && (-a == K || -a == P)) || -a == Q || -a == B ){
		  if(where != NULL){
		     acum.push_back('S');
		  }
		  ans = true;
	       }
	       break;
	    }
	 }
      }

      // arriba - derecha
      for(int i=1; posActual.first+i<8 && posActual.second-i>=0 ; i++){
	 int a = this->board[(posActual.second-i)*8+posActual.first+i];
	 if(color<0){
	    if( a < 0){
	       break;
	    }else if( a > 0 ){
	       if((i == 1 && (a == K || a == P)) || a == Q || a == B ){
		  if(where != NULL){
		     acum.push_back('S');
		  }
		  ans = true;
	       }
	       break;
	    }	 
	 }else{
	    if( a > 0){
	       break;
	    }else if( a < 0 ){
	       if((i == 1 && -a == K) || -a == Q || -a == B ){
		  if(where != NULL){
		     acum.push_back('S');
		  }
		  ans = true;
	       }
	       break;
	    }
	 }
      }


   }else{
      //revisa primero arriba-derecha y abajo- izq

      // abajo - izq
      for(int i=1; posActual.first-i>=0 && posActual.second+i<8 ; i++){
	 int a = this->board[(posActual.second+i)*8+posActual.first-i]; 
	 if(color<0){
	    if( a < 0){
	       break;
	    }else if( a > 0 ){
	       if((i == 1 && a == K) || a == Q || a == B ){
		  if(where != NULL){
		     acum.push_back('L');
		  }
		  ans = true;
	       }
	       break;
	    }	 
	 }else{
	    if( a > 0){
	       break;
	    }else if( a < 0 ){
	       if((i == 1 && (-a == K || -a == P)) || -a == Q || -a == B ){
		  if(where != NULL){
		     acum.push_back('L');
		  }
		  ans = true;
	       }
	       break;
	    }
	 }
      }

      // arriba - derecha
      for(int i=1; posActual.first+i<8 && posActual.second-i>=0 ; i++){
	 int a = this->board[(posActual.second-i)*8+posActual.first+i];
	 if(color<0){
	    if( a < 0){
	       break;
	    }else if( a > 0 ){
	       if((i == 1 && (a == K || a == P)) || a == Q || a == B ){
		  if(where != NULL){
		     acum.push_back('L');
		  }
		  ans = true;
	       }
	       break;
	    }	 
	 }else{
	    if( a > 0){
	       break;
	    }else if( a < 0 ){
	       if((i == 1 && -a == K) || -a == Q || -a == B ){
		  if(where != NULL){
		     acum.push_back('L');
		  }
		  ans = true;
	       }
	       break;
	    }
	 }
      }

            // arriba - izquierda
      for(int i=1; posActual.first-i>=0 && posActual.second-i>=0 ; i++){
	 int a = this->board[(posActual.second-i)*8+posActual.first-i];
	 if(color<0){
	    if( a < 0){
	       break;
	    }else if( a > 0 ){
	       if((i == 1 && (a == K || a == P)) || a == Q || a == B){
		  if(where != NULL){
		     acum.push_back('S');
		  }
		  ans = true;
	       }
	       break;
	    }	 
	 }else{
	    if( a > 0){
	       break;
	    }else if( a < 0 ){
	       if((i == 1 && -a == K) || -a == Q || -a == B ){
		  if(where != NULL){
		     acum.push_back('S');
		  }
		  ans = true;
	       }
	       break;
	    }
	 }
      }

      // abajo - derecha
      for(int i=1; posActual.first+i<8 && posActual.second+i<8 ; i++){
	 int a = this->board[(posActual.second+i)*8+posActual.first+i];
	 if(color<0){
	    if( a < 0){
	       break;
	    }else if( a > 0 ){
	       if((i == 1 && a == K) || a == Q || a == B ){
		  if(where != NULL){
		     acum.push_back('S');
		  }
		  ans = true;
	       }
	       break;
	    }	 
	 }else{
	    if( a > 0){
	       break;
	    }else if( a < 0 ){
	       if((i == 1 && (-a == K || -a == P)) || -a == Q || -a == B ){
		  if(where != NULL){
		     acum.push_back('S');
		  }
		  ans = true;
	       }
	       break;
	    }
	 }
      }
   }

   sort(acum.begin(), acum.end());
   
   if(where != NULL)
      where->assign(acum);
   return ans;
}

bool Cstate::attack(pair<int, int> posActual, int color, string* where){
   //rey
   for(int i = 0 ; i < 8 ; i++){
      int* j = kingPos[i];
      if(posActual.first+j[0]>=0 && posActual.first+j[0]<8 &&
	 posActual.second+j[1]>=0 && posActual.second+j[1]<8){
	 int a = this->board[(posActual.second+j[1])*8+posActual.first+j[0]];
	 
	 if(color<0){
	    if( a > 0 ){
	       if( a == K )
		  return true;
	       else if( (i == 0 || i==6) && a == P)
		  return true;
	    }	 
	 }else{
	    if( a < 0 ){
	       if( -a == K )
		  return true;
	       else if( (i == 2 || i==4) && -a == P)
		  return true;
	    }
	 }
      }
   }
   
      
   //caballo
   for(int* i : knightPos){
      if(posActual.first+i[0]>=0 && posActual.first+i[0]<8 &&
	 posActual.second+i[1]>=0 && posActual.second+i[1]<8){
	 int a = this->board[(posActual.second+i[1])*8+posActual.first+i[0]];

	 if(color<0){
	    if( a > 0 ){
	       if( a == N )
		  return true;
	    }	 
	 }else{
	    if( a < 0 ){
	       if(-a == N )
		  return true;
	    }
	 }
      }
   }
   
   //izquierda
   for(int i = posActual.first-1; i >= 0 ; i-- ){
      int a = this->board[(posActual.second)*8+i];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if(a == Q || a == R )
	       return true;
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if(-a == Q || -a == R )
	       return true;
	    break;
	 }
      }
   }

   //derecha
   for(int i = posActual.first+1; i < 8 ; i++ ){
      int a = this->board[(posActual.second)*8+i];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if(a == Q || a == R )
	       return true;
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if(-a == Q || -a == R )
	       return true;
	    break;
	 }
      }
   }

   //arriba
   for(int i = posActual.second-1; i >= 0 ; i-- ){
      int a = this->board[(i)*8+posActual.first];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if(a == Q || a == R )
	       return true;
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if(-a == Q || -a == R )
	       return true;
	    break;
	 }
      }
   }

   //abajo
   for(int i = posActual.second+1; i < 8 ; i++ ){
      int a = this->board[(i)*8+posActual.first];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if(a == Q || a == R )
	       return true;
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if(-a == Q || -a == R )
	       return true;
	    break;
	 }
      }
   }

   // abajo - derecha
   for(int i=1; posActual.first+i<8 && posActual.second+i<8 ; i++){
      int a = this->board[(posActual.second+i)*8+posActual.first+i];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if(a == Q || a == B )
	       return true;
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if(-a == Q || -a == B )
	       return true;
	    break;
	 }
      }
   }

   // abajo - izq
   for(int i=1; posActual.first-i>=0 && posActual.second+i<8 ; i++){
      int a = this->board[(posActual.second+i)*8+posActual.first-i];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if(a == Q || a == B )
	       return true;
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if(-a == Q || -a == B )
	       return true;
	    break;
	 }
      }
   }

   // arriba - derecha
   for(int i=1; posActual.first+i<8 && posActual.second-i>=0 ; i++){
      int a = this->board[(posActual.second-i)*8+posActual.first+i];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if(a == Q || a == B )
	       return true;
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if(-a == Q || -a == B )
	       return true;
	    break;
	 }
      }
   }

   // arriba - izquierda
   for(int i=1; posActual.first-i>=0 && posActual.second-i>=0 ; i++){
      int a = this->board[(posActual.second-i)*8+posActual.first-i];
      if(color<0){
	 if( a < 0){
	    break;
	 }else if( a > 0 ){
	    if(a == Q || a == B )
	       return true;
	    break;
	 }	 
      }else{
	 if( a > 0){
	    break;
	 }else if( a < 0 ){
	    if(-a == Q || -a == B )
	       return true;
	    break;
	 }
      }
   }
   
   return false;
}


int Cstate::drawBoard(int tab){

   for(int j=0;j<tab;j++){
      cout<<"\t";
   }
   
   cout << " a  b  c  d  e  f  g  h" << endl;
   int tot = 0;
   for(int j=0;j<tab;j++){
      cout<<"\t";
   }
   for(int i=0;i<64;++i){
      if(i%8 == 0){
	 cout << endl;
	 for(int j=0;j<tab;j++){
	    cout<<"\t";
	 }
      }
      cout <<( ((int) (this->board[i]))<0 ? "" : " " )<< (int) (this->board[i]) << ",";
      if(this->board[i]!=0) tot++;
   }

   cout << endl;
   for(int j=0;j<tab;j++){
      cout<<"\t";
   }

   cout << "total: " << tot << endl;
   for(int j=0;j<tab;j++){
      cout<<"\t";
   }
   cout << "extraData: " << hex << (int) this->extraData << dec << endl;
   for(int j=0;j<tab;j++){
      cout<<"\t";
   }
   cout << "zobristKey: " << this->zobristKey << endl;
   for(int j=0;j<tab;j++){
      cout<<"\t";
   }
   cout << "illegal: "<< this->illegal << endl;
   return tot;
}

int Cstate::numPieces(int color){
   int res = 0;
   for(int i=0;i<64;++i){
      if(this->board[i] > 0 && color > 0) ++res;
      else if(this->board[i] < 0 && color < 0) ++res;
   }
   return res;
}

bool Cstate::isMaterialWin(int color){
   if(numPieces(-color) != 1) return false;

   int kPos = kingData[(color == WHITE ? 1 : 0)];
   int kX = kPos % 8;
   int kY = kPos / 8;

   bool hasQueen = false;
   bool hasRook = false;
   bool hasLegalPawn = false;
   bool hasKnight = false;
   int bishops = 0;

   for(int* i : kingPos){
      if(kX+i[0] >=0 && kX+i[0] < 8 &&
	 kY+i[1] >=0 && kY+i[1] < 8){
	 int piece = this->board[kX+i[0] + (kY+i[1])*8];
	 if(piece == Q*color) hasQueen = true;
	 else if(piece == R*color) hasRook = true;
	 else if(piece == K*color) hasKnight = true;
	 else if(piece == B*color) bishops += 1;
	 else if(piece == P*color && kX+i[0] != 0 && kX+i[0] != 7){
	    hasLegalPawn = true;
	 }
      }
   }

   return hasQueen || hasRook || hasLegalPawn || bishops == 2 || (hasKnight && bishops == 1);
}
