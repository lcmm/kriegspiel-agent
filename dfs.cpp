#include "beliefState/BeliefState.h"
#include "beliefState/ChessState.h"
#include "estimador.h"
#include "Cstate.h"
#include "Nodo.h"
#include <vector>
#include <string>
#include <string.h>
#include <fstream>
#include <sstream>
#include <iostream>

int main(int argc, char** argv){
   playset p(readProblemFile(argv[1]));
   ChessState::initSeed();
   Cstate s;
   BeliefState bs(1000000);
   bs.addState(&s);
   estimate(p,&bs);

   if(argc < 4) return 0;

   Node_Or* start = new Node_Or();
   start->beliefState = bs;
   start->depth = atoi(argv[3]);

   if(strcmp(argv[2],"dfs") == 0){
      cout << "Starting DFS" << endl;
      if(start->solve_top_dfs(false)) cout << "Solved!" << endl;
      else cout << "Not_Solved" << endl;
   }else if(strcmp(argv[2],"dbu") == 0){
      cout << "Starting DBU" << endl;
      if(start->solve_top_dbu()) cout << "Solved!" << endl;
      else cout << "Not_Solved" << endl;
   }else if(strcmp(argv[2],"dub") == 0){
      cout << "Starting DUB" << endl;
      if(start->outer_top_dub()) cout << "Solved!" << endl;
      else cout << "Not_Solved" << endl;
   }else{
      cout << "Invalid algorithm: " << argv[2] << endl;
   }

   return 0;
}
