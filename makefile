all: dfs.out 

dfs.out: beliefState.o Cstate.o Nodo.o estimador.o
	 g++ -g beliefState/*.o Cstate.o Nodo.o estimador.o dfs.cpp -o dfs.out -std=c++11


estimador.o: beliefState.o Cstate.o
	 g++ -c -g beliefState/*.o Cstate.o estimador.cpp -std=c++11

beliefState.o:
	 cd beliefState && $(MAKE)

Cstate.o:
	 g++ -c -g Cstate.cpp -std=c++11

Nodo.o:
	 g++ -c -g Nodo.cpp -std=c++11

clean:
	 cd beliefState && $(MAKE) clean && cd ..
	 rm *.o
	 rm *.out
