import os
import re
import sys 

profundidad = int(sys.argv[1])
dir = "d"+str(profundidad)+"/"
valores = [0,0,0,0,0,0]

p = re.compile('\d+')
j = 0
for arch in os.listdir(dir):
    file = open(dir+arch, 'r')
    i=0
    low=13
    high=19
    for line in file:
        if i == 19:
            break
        if i >= low:
            valores[i-13]+=int(p.findall(line)[0])
        i+=1
    j+=1

valores = [round(x/j) for x in valores]
print("computed-states " + str(valores[0]))
print("final-states " + str(valores[1]))
print("mate-states " + str(valores[2]))
print("largest-mate-subset " + str(valores[3]))
print("white-pcs " + str(valores[4]))
print("black-pcs " + str(valores[5]))

print()
print("Minimo de estados fisicos " + str(profundidad * valores[4] * valores[5] * valores[1]))
