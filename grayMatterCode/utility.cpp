#include "utility.h"

static const int MultiplyDeBruijnBitPosition[32] = 
{
   1,2,29,3,30,15,25,4,31,23,21,16,26,18,5,9,
   32,28,14,24,22,20,17,8,27,13,19,7,12,6,11,10
};

int find_32(uint32_t n)
{
   if (n == 0) 
      return 0;
   else 
      return MultiplyDeBruijnBitPosition[((uint32_t)((n & -n) * 0x077CB531U)) >> 27];
}

int find_64(uint64_t n)
{
   if (n == 0) return 0;

   int c = find_32(n); 
   if (c) return c;

   return find_32(n >> 32) + 32; 
}
