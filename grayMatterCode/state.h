#include <iostream>
#include <stdint.h>
#include "utility.h"

// piece colors:
#define WHITE       0
#define BLACK       1
#define COLORS      2

// piece shapes:
#define PAWN        0
#define KNIGHT      1
#define BISHOP      2
#define ROOK        3
#define QUEEN       4
#define KING        5
#define SHAPES      6

// Castling sides:
#define QUEEN_SIDE  0
#define KING_SIDE   1
#define SIDES       2

// un bitboard para cada tipo de pieza
typedef uint64_t bitboard_t;

// encuetra la primera posicion ocupada en un bitboard
inline static int FST(bitboard_t b){return (find_64(b) - 1);}

// mascara para las filas
bitboard_t ROW_MSK[8]={0xFFULL,                //1
                       0xFF00ULL,              //2
                       0xFF0000ULL,            //3
                       0xFF000000ULL,          //4
                       0xFF00000000ULL,        //5
                       0xFF0000000000ULL,      //6
                       0xFF000000000000ULL,    //7
                       0xFF00000000000000ULL}; //8

// mascara para las posiciones
// [FILA][COLUMNA]
bitboard_t BIT_MSK[8][8]=
{
   /* H,G,F,E,D,C,B,A */
   /*1*/{0x1ULL,0x2ULL,0x4ULL,0x8ULL,0x10ULL,0x20ULL,0x40ULL,0x80ULL},
   /*2*/{0x100ULL,0x200ULL,0x400ULL,0x800ULL,0x1000ULL,0x2000ULL,0x4000ULL,0x8000ULL},
   /*3*/{0x10000ULL,0x20000ULL,0x40000ULL,0x80000ULL,0x100000ULL,0x200000ULL,0x400000ULL,0x800000ULL},
   /*4*/{0x1000000ULL,0x2000000ULL,0x4000000ULL,0x8000000ULL,0x10000000ULL,0x20000000ULL,0x40000000ULL,0x80000000ULL},
   /*5*/{0x100000000ULL,0x200000000ULL,0x400000000ULL,0x800000000ULL,0x1000000000ULL,0x2000000000ULL,0x4000000000ULL,0x8000000000ULL},
   /*6*/{0x10000000000ULL,0x20000000000ULL,0x40000000000ULL,0x80000000000ULL,0x100000000000ULL,0x200000000000ULL,0x400000000000ULL,0x800000000000ULL},
   /*7*/{0x1000000000000ULL,0x2000000000000ULL,0x4000000000000ULL,0x8000000000000ULL,0x10000000000000ULL,0x20000000000000ULL,0x40000000000000ULL,0x80000000000000ULL},
   /*8*/{0x100000000000000ULL,0x200000000000000ULL,0x400000000000000ULL,0x800000000000000ULL,0x1000000000000000ULL,0x2000000000000000ULL,0x4000000000000000ULL,0x8000000000000000ULL}
};

// cantidad de shift a una direccion que se necesita
// para llegar a una posicion
// [FILA][COLUMNA]
int BIT_IDX[8][8]=
{
   /* H,G,F,E,D,C,B,A */
   /*1*/{ 0, 1, 2, 3, 4, 5, 6, 7},
   /*2*/{ 8, 9,10,11,12,13,14,15},
   /*3*/{16,17,18,19,20,21,22,23},
   /*4*/{24,25,26,27,28,29,30,31},    
   /*5*/{32,33,34,35,36,37,38,39},
   /*6*/{40,41,42,43,44,45,46,47},
   /*7*/{48,49,50,51,52,53,54,55},
   /*8*/{56,57,58,59,60,61,62,63}
};

bitboard_t mov_king[8][8];
bitboard_t mov_bishop[8][8];
bitboard_t mov_rook[8][8];
bitboard_t mov_knight[8][8];

inline static bool BIT_GET(bitboard_t b, int x, int y) {return ((b) >> BIT_IDX[x][y] & 1);}

inline static void BIT_SET(bitboard_t& b, int x, int y) {(b) |= BIT_MSK[x][y];}

inline static void BIT_CLR(bitboard_t& b, unsigned x, unsigned y) {(b) &= ~BIT_MSK[x][y];}

void precomp_king(){
   //    bitboard_t dummy;
   for (int n = 0; n <= 63; n++){
      int x = n & 0x7;
      int y = n >> 3;
//        cout<<"\n\tx: "<<x<<" , y: "<<y<<endl;
//        dummy = 0;
      mov_king[x][y];
      for (int k = -1; k <= 1; k++)
         for (int j = -1; j <= 1; j++){
            if (!j && !k)
               continue;
            if (x + j < 0 || x + j > 7 ||
                y + k < 0 || y + k > 7)
               continue;
            //BIT_SET(dummy, y + k, x + j);
            BIT_SET(mov_king[x][y], y + k, x + j);
//                cout<<"\nx: "<<x+j<<" , y: "<<y+k;
         }        
//        cout<<endl;
   };
}

void precomp_bishop()
{
   for (int n = 0; n <= 63; n++){
      int x = n & 0x7;
      int y = n >> 3;
//          cout<<"\n\tx: "<<x<<" , y: "<<y<<endl;
//        dummy = 0;
      for (int k = -7; k <= 7; k++){
//            cout<<"\nk: "<<k;
         if (!k)
            continue;
         if (!(x + k < 0 || x + k > 7 ||
               y + k < 0 || y + k > 7)){
//                cout<<"\n\tx: "<<x+k<<" , y: "<<y+k;
//                BIT_SET(dummy, y + k, x + k);
            BIT_SET(mov_bishop[x][y], y + k, x + k);
         }
            
         if (!(x - k < 0 || x - k > 7 ||
               y + k < 0 || y + k > 7)){
//                cout<<"\n\tx: "<<x-k<<" , y: "<<y+k;
//                BIT_SET(dummy, y + k, x - k);
            BIT_SET(mov_bishop[x][y], y + k, x - k);
         }
            
      }        
//        cout<<endl;
   }
}

void precomp_rook()
{
   for (int n = 0; n <= 63; n++){
      int x = n & 0x7;
      int y = n >> 3;
//        cout<<"\n\tx: "<<x<<" , y: "<<y<<endl;
//        dummy = 0;
      for (int k = -7; k <= 7; k++){
//            cout<<"\nk: "<<k;
         if (!k)
            continue;
         if (!(x + k < 0 || x + k > 7)){
//                cout<<"\n\tx: "<<x+k<<" , y: "<<y+k;
//                BIT_SET(dummy, y , x + k);
            BIT_SET(mov_rook[x][y], y , x + k);
         }
            
         if (!(y + k < 0 || y + k > 7)){
//                cout<<"\n\tx: "<<x-k<<" , y: "<<y+k;
//                BIT_SET(dummy, y + k, x);
            BIT_SET(mov_rook[x][y], y + k, x);
         }
      }
        
//            cout<<endl;
   };
}

void precomp_knight()
{
   for (int n = 0; n <= 63; n++){
      int x = n & 0x7;
      int y = n >> 3;
//        cout<<"\n\tx: "<<x<<" , y: "<<y<<endl;
//        dummy = 0;
      for (int k = -2; k <= 2; k++)
         for (int j = -2; j <= 2; j++){
            if (!j && !k)
               continue;
            if (abs(j)+abs(k)!=3)
               continue;
            if (x + j < 0 || x + j > 7 ||
                y + k < 0 || y + k > 7)
               continue;
            //BIT_SET(dummy, y + k, x + j);
            BIT_SET(mov_knight[x][y], y + k, x + j);
//                cout<<"\nx: "<<x+j<<" , y: "<<y+k;
         }        
//        cout<<endl;
   };
}


class State
{
public:
   bitboard_t pieces[COLORS][SHAPES];
   int castle[COLORS][SIDES];
   int en_passant;

   void initialize(){
      pieces[WHITE][PAWN]=0xFF00ULL;
      pieces[WHITE][KNIGHT]=0x42ULL;
      pieces[WHITE][BISHOP]=0x24ULL;
      pieces[WHITE][ROOK]=0x81ULL;
      pieces[WHITE][QUEEN]=0x10ULL;
      pieces[WHITE][KING]=0x8ULL;

      castle[WHITE][QUEEN_SIDE]=1;
      castle[WHITE][KING_SIDE]=1;
    
      pieces[BLACK][PAWN]=0xFF000000000000ULL;
      pieces[BLACK][KNIGHT]=0x4200000000000000ULL;
      pieces[BLACK][BISHOP]=0x2400000000000000ULL;
      pieces[BLACK][ROOK]=0x8100000000000000ULL;
      pieces[BLACK][QUEEN]=0x1000000000000000ULL;
      pieces[BLACK][KING]=0x800000000000000ULL;

      castle[BLACK][QUEEN_SIDE]=1;
      castle[BLACK][KING_SIDE]=1;

      en_passant = 0;
   };
    
};
