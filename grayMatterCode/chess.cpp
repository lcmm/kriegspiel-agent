#include<iostream>
#include<algorithm>
#include<math.h>
#include <fstream>
#include "state.h"
//#include "utility.h"
//#include "movimientos.h"
using namespace std;

ostream& operator<<(ostream& out, const State dummy)
{
   int x,y,color,shape;
   out << "    a   b   c   d   e   f   g   h" << endl;
   out << "  +---+---+---+---+---+---+---+---+" << endl;
   for(y=7;y>=0;y--){
      out << y+1 <<" ";
      for(x=0;x<8;x++){
	 for(shape=PAWN;shape<=KING;shape++){
	    if(BIT_GET(dummy.pieces[WHITE][shape],y,x)){
	       color = WHITE;
	       break;
	    }
	    if(BIT_GET(dummy.pieces[BLACK][shape],y,x)){
	       color = BLACK;
	       break;
	    }
	 }
	 switch (shape)
	 {              
	 case PAWN   : out << (color == WHITE ? "| P " : "| p "); break;
	 case KNIGHT : out << (color == WHITE ? "| N " : "| n "); break;
	 case BISHOP : out << (color == WHITE ? "| B " : "| b "); break;
	 case ROOK   : out << (color == WHITE ? "| R " : "| r "); break;
	 case QUEEN  : out << (color == WHITE ? "| Q " : "| q "); break;
	 case KING   : out << (color == WHITE ? "| K " : "| k "); break;
	 default     : out << "|   "; break;
	 }
      }
      out << "| " <<y+1<< endl;
      out << "  +---+---+---+---+---+---+---+---+" << endl;
   }
   out << "    a   b   c   d   e   f   g   h" << endl;
   return out;
}

void printBitboard(bitboard_t dummy)
{
   int x1,y1;
   cout << "    a   b   c   d   e   f   g   h" << endl;
   cout << "  +---+---+---+---+---+---+---+---+" << endl;
   for(y1=7;y1>=0;y1--){
      cout << y1+1 <<" ";
      for(x1=0;x1<8;x1++){
	 if(BIT_GET(dummy,y1,x1))
	    cout << "| x ";
	 else
	    cout << "|   ";
      }
      cout << "| " <<y1+1<< endl;
      cout << "  +---+---+---+---+---+---+---+---+" << endl;
   }
   cout << "    a   b   c   d   e   f   g   h" << endl;
}



main()
{
   State welp;
   welp.initialize();
//    for(int i=PAWN;i<=KING;i++)
   // cout<<(FST(welp.pieces[WHITE][KING]))
   //     <<" = "
   //     <<(FST(welp.pieces[WHITE][KING])&0x7)
   //     <<","
   //     <<(FST(welp.pieces[WHITE][KING])>>3)<<endl;

   // cout << welp << endl;

   precomp_king();
   precomp_bishop();
   precomp_rook();
   precomp_knight();
    
   bitboard_t b = welp.pieces[WHITE][KNIGHT] | welp.pieces[BLACK][KNIGHT];
            
   for(int n, x, y; (n = FST(b)) != -1; BIT_CLR(b, y, x)){
      x = n & 0x7;
      y = n >> 3;
      cout<<"\tx: "<<x<<" , y: "<<y<<endl;
      printBitboard(mov_knight[x][y]);
   }
    
    
   bitboard_t dummy;
        
   //        cout<<dummy<<endl;
}

    
